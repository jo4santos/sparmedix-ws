<?php
if(!class_exists("products")){
	class products
	{
	    public function __construct() {
			
	    }
	    
	    public function process() {
			global $API;
        	if ((int)method_exists($this, $API->method) > 0) {
        		return $this->{$API->method}($API->args);
        		if($retval!=false)
					return $API->_response($retval);
        		else
        			return $API->_response("Internal error: $API->method", 500);
        	}
        	return $API->_response("No method: $API->method", 405);
	    }
	    
		protected function put() {
			global $API;

			set_time_limit(0);
			
			$products = array();
			if(isset($API->put_contents["products"]))
				$products = $API->put_contents["products"];

			if(!is_array($products)) {
				$products = explode("_spout_",$products);
				foreach($products as &$product) {
					$fields = explode("_spin_",$product);
					$product = array();
					$product["codigo"] = $fields[0];
					$product["nome"] = $fields[1];
					$product["tipo_codigo"] = $fields[2];
					$product["preco"] = $fields[3];
					sp_utils_format_price($product["preco"]);
					$product["bonus"] = $fields[4];
					$product["iva"] = $fields[5];
					$product["familia"] = $fields[6];
                    $product["pvalsiva"] = $fields[7];
                    sp_utils_format_price($product["pvalsiva"]);
                    $product["pvp3siva"] = $fields[8];
                    sp_utils_format_price($product["pvp3siva"]);
					$product["action"] = $fields[9];

					foreach ($product as &$val) {
						$val = urlencode($val);
					}
				}
			}

			unset($product);
			
			$initial_update = false;
			if(isset($API->put_contents["initial_update"]))
				$initial_update = true;
			
			$query = "select codigo from products";
			$result = $API->dbh->query($query);
			if ( $result ) {
				$elements = $result->fetchAll(PDO::FETCH_ASSOC);
			}
			$codigos = array();
			foreach($elements as $element) {
				$codigos[$element["codigo"]]=true;
			}

			if($initial_update) {
				try{
					$stmt = $API->dbh->exec("UPDATE products SET updated_db='0';");
				}
				catch(PDOException $e) {
					return $API->_response("Erro ao atualizar produto na base de dados", 500);
				}
			}			
			
			$API->dbh->beginTransaction();
			foreach($products as $product) {
				if ( isset($codigos[$product["codigo"]]) ) {
					// Existe
					if ( $product["action"] == "d" )
					{
						try
						{
							$stmt = $API->dbh->exec("DELETE FROM products WHERE codigo='".$product["codigo"]."';");
						}
						catch(PDOException $e) {
							return $API->_response("Erro ao atualizar produto na base de dados", 500);
						}
					}
					else
					{
						try
						{
							$stmt = $API->dbh->exec("UPDATE products SET tipo_codigo='".$product["tipo_codigo"]."',codigo='".$product["codigo"]."',nome='".$product["nome"]."',preco='".$product["preco"]."',bonus='".$product["bonus"]."',iva='".$product["iva"]."',familia='".$product["familia"]."',pvalsiva='".$product["pvalsiva"]."',pvp3siva='".$product["pvp3siva"]."',updated_db='1' WHERE codigo='".$product["codigo"]."'");
						}
						catch(PDOException $e) {
							return $API->_response("Erro ao atualizar produto na base de dados", 500);
						}
					}
				}
				else {
					// Nao existe
					if ( $product["action"] != "d" )
					{
						try
						{
							$stmt = $API->dbh->exec("INSERT INTO products (tipo_codigo,codigo,nome,preco,bonus,iva,familia,pvalsiva,pvp3siva,updated_db) VALUES ('".$product["tipo_codigo"]."','".$product["codigo"]."','".$product["nome"]."','".$product["preco"]."','".$product["bonus"]."','".$product["iva"]."','".$product["familia"]."','".$product["pvalsiva"]."','".$product["pvp3siva"]."','1')");
						}
						catch(PDOException $e) {
							return $API->_response("Erro ao inserir produto na base de dados", 500);
						}
					}
				}
			}
			$API->dbh->commit();
			
			if($initial_update) {
				try{
					$stmt = $API->dbh->exec("DELETE FROM products WHERE updated_db='0.0';");
				}
				catch(PDOException $e) {
					return $API->_response("Erro ao atualizar produto na base de dados", 500);
				}
			}
			
			return $API->_response("");
		}
	    
		protected function get() {
			global $API;

            /*try{
                $stmt = $API->dbh->exec("ALTER TABLE products ADD COLUMN pvp3siva VARCHAR(256) DEFAULT '0';");
                return $API->_response("OK");
            }
            catch(PDOException $e) {
                return $API->_response("NOK", 500);
            }
            return $API->_response("Adicionada coluna", 500);*/

			if ( count ( $API->args ) > 0 ) {
				if ( !is_numeric ( $API->args[0] ) ) return $API->_response("Id de produto invalido ".$API->args[0], 400);
				if ( !in_array($API->args[0],$API->user["produtos"]) ) return $API->_response("Nao autorizado", 403);
				$result = $API->dbh->query("select * from products where id='".$API->args[0]."'");
				if ( $result )	{
					$result = $result->fetch(PDO::FETCH_ASSOC);
					if ( $result )
					{
						foreach ($result as &$val) $val = urldecode($val);
                        sp_utils_format_price($result["preco"]);
                        sp_utils_format_price($result["pvalsiva"]);
                        sp_utils_format_price($result["pvp3siva"]);
						return $API->_response($result);
					}
					else {
						return $API->_response("Produto nao encontrado: id ".$API->args[0], 404);
					}
				}
			}
			else {
				$query = "select * from products";
				if(count($API->user["produtos"])>0)
					$query.=" where id in ('".implode("','",$API->user["produtos"])."')";
				$query .= " order by nome ASC";
				$result = $API->dbh->query($query);
				if ( $result ) {
					$elements = $result->fetchAll(PDO::FETCH_ASSOC);
					if( count ( $elements ) > 0 ) {
						foreach ($elements as &$element) {
							foreach ($element as &$val) $val = urldecode($val);
                            sp_utils_format_price($element["preco"]);
                            sp_utils_format_price($element["pvalsiva"]);
                            sp_utils_format_price($element["pvp3siva"]);
						}
						return $API->_response($elements);
					}
					else {
						return $API->_response(array());
					}
				}
			}
			return $API->_response("Erro ao obter informacoes de produtos", 500);
		}
		
		protected function post() {
			global $API;

			return $API->_response("No method: $API->method", 405);
		}
		
		protected function delete() {
			global $API;

			return $API->_response("No method: $API->method", 405);
		}
	}
}
?>