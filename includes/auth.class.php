<?php
if(!class_exists("auth")){
	class auth
	{
		var $prefix = '$2a$';
		var $custo = 10;
		var $salt = 'h1s2a3h4s5i6d7e8m9raps';
	    public function __construct() {
			
	    }
	    
	    public function process() {
			global $API;
        	if ((int)method_exists($this, $API->method) > 0) {
        		return $this->{$API->method}($API->args);
        	}
        	return $API->_response("No method: $API->method", 405);
	    }
	    
		protected function put() {
			global $API;

			$id = "";
			if(isset($API->put_contents["id"]))
				$id = $API->put_contents["id"];
			
			return $API->_response(print_r($API->put_contents,true));
		}
	    
		protected function get() {
			global $API;
			if(count($API->args)>0)
			{
				if($API->args[0]=="pages") {
					$pages = array();
					$pages[] = array("nome"=>"Inicio","url"=>"#dashboard","icon"=>"fa-home","list"=>1);
					$pages[] = array("nome"=>"Encomenda","url"=>"#order","list"=>0);
					$pages[] = array("nome"=>"Notificações","url"=>"#notifications","list"=>0);
					$pages[] = array("nome"=>"Aplicação Android","url"=>"#android","list"=>0);
					$pages[] = array("nome"=>"Sincronização","url"=>"#sync","list"=>0);
					switch($API->user["tipo"]) {
						case 0:
						{
							$pages[] = array("nome"=>"Utilizadores","url"=>"#users","icon"=>"fa-user","list"=>1);
						}
						case 1: 
						{
							$pages[] = array("nome"=>"Clientes","url"=>"#clients","icon"=>"fa-suitcase","list"=>1);
						}
						case 2:
						default: 
						{
							$pages[] = array("nome"=>"Produtos","url"=>"#products","icon"=>"fa-list","list"=>1);
							$pages[] = array("nome"=>"Encomendas","url"=>"#notes","icon"=>"fa-shopping-cart","list"=>1);
						}
					}
					return $API->_response($pages);
				}
			}

			$configs_to_read = array("email_notification","automatic_sync");
			$configs = array();
			$user["configs"] = array();
			foreach ($configs_to_read as $config) {
				$result = $API->dbh->query("select * from configurations where name='".$config."_default'");
				if ( $result )	{
					$result = $result->fetch(PDO::FETCH_ASSOC);
					if ( $result )
					{
						$configs[$config] = $result;
					}
				}
				$result = $API->dbh->query("select * from configurations where user_id='".$API->user["id"]."' AND name='".$config."'");
				if ( $result )	{
					$result = $result->fetch(PDO::FETCH_ASSOC);
					if ( $result )
					{
						$configs[$config] = $result;
					}
				}
			}

			$apk_version = "";
			$uploaddir = dirname(__FILE__)."/../../apk/release/";
            if(file_exists($uploaddir)) {
                if ($handle = opendir($uploaddir)) {
                    while (false !== ($filename = readdir($handle))) {
                        $extension = strtolower(end(explode(".",$filename)));
                        if ($extension == "apk") {
                            $apk_version = $filename;
                            $apk_version = str_replace("sparmedix-", "", $apk_version);
                            $apk_version = str_replace(".apk", "", $apk_version);
                            break;
                        }
                    }
                    closedir($handle);
                }
            }

			return $API->_response(array("id"=>$API->user["id"],"apk_version"=>$apk_version,"configs"=>$configs,"fav_clients"=>$this->get_user_fav_clients($API->user["id"]),"fav_products"=>$this->get_user_fav_products($API->user["id"]),"nome"=>$API->user["nome"],"tipo"=>$API->user["tipo"],"email"=>$API->user["email"],"session_start"=>$API->user["session_start"],"client_id"=>$API->user["client_id"]));
		}
		
		protected function post() {
			global $API;
			
			$email = $API->request["e"];

			if(count($API->args)>0)
			{
				if($API->args[0]=="recover") {
					$result = $API->dbh->query("select id,nome,email,tipo,grupo from users where email='".$email."'");
					if ( $result )	{
						$result = $result->fetch(PDO::FETCH_ASSOC);
						if ( $result )
						{
							$id = $result["id"];
							$new_password = sp_utils_generate_random(8);
							$time = time();
							$hash = crypt($new_password, $this->prefix . 10 . '$' . $this->salt . '$');
							try{
								$stmt = $API->dbh->prepare("UPDATE users SET password=? WHERE id='$id'");
								$stmt->bindParam(1, $hash);
								$stmt->execute();
							}
							catch(PDOException $e) {
								$API->log_action(array(
									"type" => "recover",
									"status" => 0,
									"user_id" => $id,
									"comment" => "Erro ao editar BD|".$email
								));
								return $API->_response("Database error in edit", 500);
							}
			
							$email_content = file_get_contents("includes/utils/email-templates/recover.html");
							$email_content = mb_convert_encoding($email_content, 'HTML-ENTITIES', "UTF-8");
							$replace = array();
							$replace["@SP_VALOR_USER_NOME@"] = $result["nome"];
							$replace["@SP_VALOR_USER_EMAIL@"] = $result["email"];
							$replace["@SP_VALOR_USER_PASSWORD@"] = $new_password;
							$replace["@SP_VALOR_PLATFORM_URL_PROFILE@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/index.html##modal#profile#open";
							$replace["@SP_VALOR_PLATFORM_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/index.html";

							$email_content = str_replace(array_keys($replace), array_values($replace), $email_content);

							$images = array();
							$images[] = array("path"=>realpath(dirname($_SERVER['SCRIPT_FILENAME']))."/../includes/utils/email-templates/logo.png","name"=>'logo.png',"cid"=>'logo.png');

							if(sp_send_email($result["email"],"web@sparmedix.pt","Web Sparmedix", "Recuperação de password", $email_content, array(), $images)) {
								return $API->_response("OK");
							} else {
								return $API->_response("Error sending e-mail", 500);
							}
						}
						else {
							return $API->_response("E-mail not found", 401);
						}
					}
					else {
						return $API->_response("E-mail not found", 401);
					}					
				}
			}
			
			$pass = $API->request["p"];

			$result = $API->dbh->query("select id,nome,email,tipo,grupo from users where password='".$pass."' and email='".$email."'");
			if ( $result )	{
				$result = $result->fetch(PDO::FETCH_ASSOC);
				if ( $result )
				{
					$id = $result["id"];
					$time = time();
					$hash = crypt($id.$time.mt_rand(0,9999999), $this->prefix . 10 . '$' . $this->salt . '$');
					try{
						$stmt = $API->dbh->prepare("UPDATE users SET hash=?,session_start=? WHERE id='$id'");
						$stmt->bindParam(1, $hash);
						$stmt->bindParam(2, $time);
						$stmt->execute();
					}
					catch(PDOException $e) {
						$API->log_action(array(
							"type" => "login",
							"status" => 0,
							"user_id" => $id,
							"comment" => "Erro ao editar BD|".$email
						));
						return $API->_response("Database error in edit", 500);
					}

					$API->log_action(array(
						"type" => "login",
						"status" => 1,
						"user_id" => $id
					));

					sp_utils_cookies_set ("auth-token", $hash, 60/*s*/*60/*m*/*24/*h*/*7/*d*/,"/ws/",'',false,true);
					return $API->_response(array("utilizador"=>array("id"=>$id,"nome"=>$result["nome"],"email"=>$result["email"],"tipo"=>$result["tipo"],"hash"=>$hash)), 200);
				}
				else
				{
					$API->log_action(array(
						"type" => "login",
						"status" => 0,
						"comment" => "Credenciais invalidas|".$email
					));
					return $API->_response("Credenciais invalidas", 401);
				}
			}
			else
			{
				$API->log_action(array(
					"type" => "login",
					"status" => 0,
					"comment" => "Utilizador nao conhecido|".$email
				));
				return $API->_response("Utilizador nao conhecido", 404);
			}
			
			$API->log_action(array(
				"type" => "login",
				"status" => 0,
				"comment" => "Erro a processar o pedido|".$email
			));
			return $API->_response("Erro a processar o pedido", 500);
		}
		
		protected function delete() {
			global $API;
			
			try{
				$stmt = $API->dbh->prepare("UPDATE users SET hash=null,session_start=null WHERE id='".$API->user["id"]."'");
				$stmt->execute();
			}
			catch(PDOException $e) {
				$API->log_action(array(
					"type" => "logout",
					"status" => 0,
					"user_id" => $API->user["id"],
					"comment" => "Erro ao remover a sessao"
				));
				return $API->_response("Erro ao remover a sessao", 500);
			}
			if(!sp_utils_cookies_remove ("auth-token","/ws/")) {
				$API->log_action(array(
					"type" => "logout",
					"status" => 0,
					"user_id" => $API->user["id"],
					"comment" => "Erro ao remover a sessao por cookie"
				));
				return $API->_response("Erro ao remover a sessao", 500);
			}
			
			$API->log_action(array(
				"type" => "logout",
				"status" => 1,
				"user_id" => $API->user["id"],
				"comment" => "Sessao terminada com sucesso"
			));
			return $API->_response("Sessao terminada com sucesso");
		}

		protected function get_user_fav_clients ($id) {
			global $API;			
			$result = $API->dbh->query("select * from users_fav_clients WHERE user_id='".$id."'");
			if ( $result ) {
				$clients = $result->fetchAll(PDO::FETCH_ASSOC);
				if( count ( $clients ) > 0 ) {
					$client_ids = array();
					foreach ( $clients as $client ) {
						$result = $API->dbh->query("select * from clients where id='".$client["client_id"]."'");
						if ( $result )	{
							$result = $result->fetch(PDO::FETCH_ASSOC);
							if ( $result )
							{
								$client_ids[] = $client["client_id"];
							}
							else 
							{
								try{
									$stmt = $API->dbh->prepare("DELETE FROM users_fav_clients WHERE client_id=?");
									$stmt->bindParam(1, $client["client_id"]);
									$stmt->execute();
								}
								catch(PDOException $e) {error_lgo("Erro da base de dados ao atualizar clientes favoritos");}
							}
						}
					}
					return $client_ids;
				}
				else {
					return array();
				}
			}
			return array();
		}

		protected function get_user_fav_products ($id) {
			global $API;			
			$result = $API->dbh->query("select * from users_fav_products WHERE user_id='".$id."'");
			if ( $result ) {
				$products = $result->fetchAll(PDO::FETCH_ASSOC);
				if( count ( $products ) > 0 ) {
					$product_ids = array();
					foreach ( $products as $product ) {
						$result = $API->dbh->query("select * from products where id='".$product["product_id"]."'");
						if ( $result )	{
							$result = $result->fetch(PDO::FETCH_ASSOC);
							if ( $result )
							{
								$product_ids[] = $product["product_id"];
							}
							else 
							{
								try{
									$stmt = $API->dbh->prepare("DELETE FROM users_fav_products WHERE product_id=?");
									$stmt->bindParam(1, $product["product_id"]);
									$stmt->execute();
								}
								catch(PDOException $e) {error_lgo("Erro da base de dados ao atualizar produtos favoritos");}
							}
						}
					}
					return $product_ids;
				}
				else {
					return array();
				}
			}
			return array();
		}
	}
}
?>