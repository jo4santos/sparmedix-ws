<?php
if(!class_exists("clients")){
	class clients
	{
	    public function __construct() {
			
	    }
	    
	    public function process() {
			global $API;
        	if ((int)method_exists($this, $API->method) > 0) {
        		return $this->{$API->method}($API->args);
        		if($retval!=false)
					return $API->_response($retval);
        		else
        			return $API->_response("Internal error: $API->method", 500);
        	}
        	return $API->_response("No method: $API->method", 405);
	    }
	    
		protected function put() {
			global $API;
			
			set_time_limit(0);
			
			$clients = array();
			if(isset($API->put_contents["clients"]))
				$clients = $API->put_contents["clients"];

			if(!is_array($clients)) {
				$clients = explode("_spout_",$clients);
				foreach($clients as &$client) {
					$fields = explode("_spin_",$client);
					$client = array();
					$client["codigo"] = $fields[0];
					$client["nome"] = urldecode($fields[1]);
					$client["nif"] = $fields[2];
					$client["responsavel"] = $fields[3];
					$client["morada"] = $fields[4];
					$client["email"] = $fields[5];
					$client["local"] = $fields[6];
					$client["cdpostal"] = $fields[7];
                    $client["tppreco"] = $fields[8];
                    $client["action"] = $fields[9];
					foreach ($client as &$val) {
						$val = urlencode($val);
					}
				}
			}

            unset($client);

			$initial_update = false;
			if(isset($API->put_contents["initial_update"]))
				$initial_update = true;
			
			$query = "select codigo from clients";
			$result = $API->dbh->query($query);
			if ( $result ) {
				$elements = $result->fetchAll(PDO::FETCH_ASSOC);
			}
			$codigos = array();
			foreach($elements as $element) {
				$codigos[$element["codigo"]]=true;
			}

			if($initial_update) {
				try{
					$stmt = $API->dbh->exec("UPDATE clients SET updated_db='0';");
				}
				catch(PDOException $e) {
					return $API->_response("Erro ao atualizar cliente na base de dados", 500);
				}
			}
			
			$API->dbh->beginTransaction();
			foreach($clients as $client) {
				if ( isset($codigos[$client["codigo"]]) ) {
					// Existe
					if ( $client["action"] == "d" )
					{
						try
						{
							$stmt = $API->dbh->exec("DELETE FROM clients WHERE codigo='".$client["codigo"]."';");
						}
						catch(PDOException $e) 
						{
							return $API->_response("Erro ao atualizar cliente na base de dados", 500);
						}
					}
					else
					{
						try
						{
							$stmt = $API->dbh->exec("UPDATE clients SET nome='".$client["nome"]."',nif='".$client["nif"]."',responsavel='".$client["responsavel"]."',morada='".$client["morada"]."',email='".$client["email"]."',local='".$client["local"]."',cdpostal='".$client["cdpostal"]."',tppreco='".$client["tppreco"]."',updated_db='1' WHERE codigo='".$client["codigo"]."'");
						}
						catch(PDOException $e) {
							return $API->_response("Erro ao atualizar cliente na base de dados", 500);
						}
					}
				}
				else {
					// Nao existe
					if ( $client["action"] != "d" )
					{
						try
						{
							$stmt = $API->dbh->exec("INSERT INTO clients (nome,codigo,nif,responsavel,morada,email,local,cdpostal,tppreco,updated_db) VALUES ('".$client["nome"]."','".$client["codigo"]."','".$client["nif"]."','".$client["responsavel"]."','".$client["morada"]."','".$client["email"]."','".$client["local"]."','".$client["cdpostal"]."','".$client["tppreco"]."','1')");
						}
						catch(PDOException $e) 
						{
							return $API->_response("Erro ao inserir cliente na base de dados", 500);
						}
					}
				}
			}
			$API->dbh->commit();
			
			if($initial_update) {
				try{
					$stmt = $API->dbh->exec("DELETE FROM clients WHERE updated_db='0.0';");
				}
				catch(PDOException $e) {
					return $API->_response("Erro ao atualizar cliente na base de dados", 500);
				}
			}
			
			return $API->_response("");
		}
	    
		protected function get() {
			global $API;


            /*try{
                $stmt = $API->dbh->exec("ALTER TABLE clients ADD COLUMN tppreco VARCHAR(10) DEFAULT '0';");
                return $API->_response("OK");
            }
            catch(PDOException $e) {
                return $API->_response("NOK", 500);
            }
            return $API->_response("Adicionada coluna", 500);*/

			if ( count ( $API->args ) > 0 ) {
				if ( !is_numeric ( $API->args[0] ) ) return $API->_response("Id de cliente invalido ".$API->args[0], 400);
				if ( !in_array($API->args[0],$API->user["clientes"]) ) return $API->_response("Nao autorizado", 403);
				$result = $API->dbh->query("select * from clients where id='".$API->args[0]."'");
				if ( $result )	{
					$result = $result->fetch(PDO::FETCH_ASSOC);
					if ( $result )
					{
						foreach ($result as &$val) $val = urldecode($val);
						return $API->_response($result);
					}
					else {
						return $API->_response("Cliente nao encontrado: id ".$API->args[0], 404);
					}
				}
			}
			else {
				$query = "select * from clients";
				if(count($API->user["clientes"])>0)
					$query.=" where id in ('".implode("','",$API->user["clientes"])."')";
				$query .= " order by nome ASC";
				$result = $API->dbh->query($query);
				if ( $result ) {
					$elements = $result->fetchAll(PDO::FETCH_ASSOC);
					if( count ( $elements ) > 0 ) {
						foreach ($elements as &$element) foreach ($element as &$val) $val = urldecode($val);
						return $API->_response($elements);
					}
					else {
						return $API->_response(array());
					}
				}
			}
			return $API->_response("Erro ao obter informacoes de clientes", 500);
		}
		
		protected function post() {
			global $API;

			return $API->_response("No method: $API->method", 405);
		}
		
		protected function delete() {
			global $API;

			return $API->_response("No method: $API->method", 405);
		}
	}
}
?>