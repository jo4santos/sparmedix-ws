<?php
if(!class_exists("images")){
	class images
	{
	    public function __construct() {
			
	    }
	    
	    public function process() {
			global $API;
        	if ((int)method_exists($this, $API->method) > 0) {
        		return $this->{$API->method}($API->args);
        		if($retval!=false)
					return $API->_response($retval);
        		else
        			return $API->_response("Internal error: $API->method", 500);
        	}
        	return $API->_response("No method: $API->method", 405);
	    }
		
		protected function get() {
			global $API;

			$uploaddir = dirname(__FILE__)."/images/";

			if(isset($API->args[0])) {
				$file = $API->args[0];

				if(isset($API->args[1])) {
					$action = $API->args[1];
					if($action=="display") {
						if(!file_exists($uploaddir.$file)) {
							$uploaddir = dirname(__FILE__)."/../../areacliente/images/";
							$file = "fallback.png";
						}

						header('Content-Type: image/JPEG');
						header('Content-Disposition: inline; filename="' . $file . '"');
						header('Content-Length: ' . filesize($uploaddir.$file));
						header('Accept-Ranges: bytes');

						@readfile($uploaddir.$file);
						return true;
					}
				}
				//return $API->_response("No method: $API->method", 405);
				header('Content-Description: File Transfer');
				header('Content-Type: image/JPEG');
				header('Content-Disposition: attachment; filename="' . $file . '"');
				header('Content-Transfer-Encoding: binary');
				header('Content-Length: ' . filesize($uploaddir.$file));
				header('Accept-Ranges: bytes');

				@readfile($uploaddir.$file);
				return true;
			}

			$query = "select * from products";
			if(count($API->user["produtos"])>0)
				$query.=" where id in ('".implode("','",$API->user["produtos"])."')";
			$query .= " order by nome ASC";
			$result = $API->dbh->query($query);
			$product_codes = array();
			if ( $result ) {
				$products = $result->fetchAll(PDO::FETCH_ASSOC);
				if( count ( $products ) > 0 ) {
					foreach ($products as &$element) {
						foreach ($element as &$val) $val = urldecode($val);
						$product_codes[] = $element["codigo"].".jpg";
					}
				}
				else {
					return $API->_response(array());
				}
			}

			//print_r($product_codes);

			$images = array();
			if ($handle = opendir($uploaddir)) {
        		while (false !== ($entry = readdir($handle))) {
        			$extension = strtolower(end(explode(".",$entry)));
            		if ($extension == "jpg" || $extension == "jpeg") {
            			$type = pathinfo($uploaddir.$entry, PATHINFO_EXTENSION);
						$data = file_get_contents($uploaddir.$entry);
						$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            			//$images[$entry] = $base64;
            			if(in_array($entry,$product_codes))
            				$images[$entry] = array("name"=>$entry,"filemtime"=>(string)filemtime ($uploaddir.$entry),"filesize"=>(string)filesize($uploaddir.$entry),"action"=>"");
            		}
        		}
        		closedir($handle);
   			}
			

			return $API->_response($images);
		}
		
		protected function post() {
			global $API;

			$imagesdir = dirname(__FILE__)."/images/";
			$uploaddir = dirname(__FILE__)."/../../areacliente/images/";

			array_map('unlink', glob( "$imagesdir*.jpg"));

			if (is_uploaded_file($_FILES['file']['tmp_name'])) {
				$uploadfile = $uploaddir . basename($_FILES['file']['name']);
				//error_log("File ". $_FILES['file']['name'] ." uploaded successfully. ");


				if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
					$zip = new ZipArchive();
					$x = $zip->open($uploadfile); // open the zip file to extract
					if ($x === true) {
						$zip->extractTo($imagesdir); // place in the directory with same name
						$zip->close();
					}
					//error_log("File is valid, and was successfully moved. ");
					return $API->_response("OK");

				}
				else {
					error_log(print_r($_FILES,true));
					return $API->_response("Error uploading",500);
				}
			}
			else {
				error_log("Upload Failed!!!");
				error_log(print_r($_FILES,true));
				return $API->_response("Error uploading",500);
			}

			return $API->_response("OK");
		}
	}
}
?>