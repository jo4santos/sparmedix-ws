<?php
date_default_timezone_set("Europe/Lisbon");

abstract class API
{
    /**
     * Property: method
     * The HTTP method this request was made in, either GET, POST, PUT or DELETE
     */
    public $method = '';
    /**
     * Property: endpoint
     * The Model requested in the URI. eg: /files
     */
    public $endpoint = '';
    /**
     * Property: verb
     * An optional additional descriptor about the endpoint, used for things that can
     * not be handled by the basic methods. eg: /files/process
     */
    public $verb = '';
    /**
     * Property: args
     * Any additional URI components after the endpoint and verb have been removed, in our
     * case, an integer ID for the resource. eg: /<endpoint>/<verb>/<arg0>/<arg1>
     * or /<endpoint>/<arg0>
     */
    public $args = Array();

    public $put_contents = Array();

    public $dbh = null;

    public $status = 200;
    public $root_public = '/';
    public $root_private = '/';

    /**
     * Constructor: __construct
     * Allow for CORS, assemble and pre-process the data
     */
    public function __construct($request)
    {


        $request = str_replace("/ws", "", $request);

        $this->args = explode('/', trim($request, '/'));

        $this->endpoint = array_shift($this->args);
//         if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
//             $this->verb = array_shift($this->args);
//         }

        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }
        $this->endpoint = strtolower($this->endpoint);
        $this->method = strtolower($this->method);

        while (!file_exists(getcwd() . "/.htroot")) {
            chdir('..');
        }
        $admin_root_private = getcwd() . "/";
        $admin_root_public = str_replace($_SERVER['DOCUMENT_ROOT'], "", $admin_root_private);
        if ($admin_root_public == "") $admin_root_public = "/";
        if ($admin_root_public[0] != '/') $admin_root_public = "/" . $admin_root_public;

        $this->root_private = $admin_root_private;
        $this->root_public = str_replace("/htdocs/public/www/", "/", $admin_root_public);
        $this->dbh = new PDO('sqlite:' . $this->root_private . 'database/database.sqlite');

        switch ($this->method) {
            case 'delete':
            case 'post':
                $this->request = $this->_cleanInputs($_POST);
                if (isset($this->request["method"])) {
                    if ($this->request["method"] == "put") {
                        $this->method = "put";
                        $this->put_contents = $this->request;
                    } else if ($this->request["method"] == "delete") {
                        $this->method = "delete";
                    }
                }
                break;
            case 'get':
                $this->request = $this->_cleanInputs($_GET);
                break;
            case 'put':
                $this->request = $this->_cleanInputs($_GET);
                parse_str(file_get_contents('php://input'), $this->put_contents);
                break;
            default:
                $this->_response('Invalid Method', 405);
                break;
        }
    }

    public function _response($data, $status = 200)
    {
        header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));
        return json_encode($data);
    }

    private function _cleanInputs($data)
    {
        $clean_input = Array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->_cleanInputs($v);
            }
        } else {
            $clean_input = trim(strip_tags($data));
        }
        return $clean_input;
    }

    private function _requestStatus($code)
    {
        $status = array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            409 => 'Conflict',
            500 => 'Internal Server Error',
        );
        return ($status[$code]) ? $status[$code] : $status[500];
    }

    public function processAPI()
    {
        if ((int)method_exists($this, $this->endpoint) > 0) {
            return $this->{$this->endpoint}($this->args);
        }

        return $this->_response("No Endpoint: $this->endpoint", 404);
    }

    public function log_action($fields = array())
    {
        global $API;
        if (!isset($fields["product_id"])) $fields["product_id"] = "";
        if (!isset($fields["client_id"])) $fields["client_id"] = "";
        if (!isset($fields["group_id"])) $fields["group_id"] = "";
        if (!isset($fields["user_id"])) $fields["user_id"] = "";
        if (!isset($fields["note_id"])) $fields["note_id"] = "";
        if (!isset($fields["comment"])) $fields["comment"] = "";
        if (!isset($fields["status"])) $fields["status"] = "";
        if (!isset($fields["type"])) $fields["type"] = "";

        try {
            $stmt = $this->dbh->prepare("INSERT INTO actions (product_id,client_id,group_id,user_id,note_id,comment,status,type) VALUES (?,?,?,?,?,?,?,?)");
            $stmt->bindParam(1, $fields["product_id"]);
            $stmt->bindParam(2, $fields["client_id"]);
            $stmt->bindParam(3, $fields["group_id"]);
            $stmt->bindParam(4, $fields["user_id"]);
            $stmt->bindParam(5, $fields["note_id"]);
            $stmt->bindParam(6, $fields["comment"]);
            $stmt->bindParam(7, $fields["status"]);
            $stmt->bindParam(8, $fields["type"]);
            $stmt->execute();
        } catch (PDOException $e) {
            return $API->_response("Erro de BD ao inserir acção", 500);
        }
        return;
    }

    function get_note_products($id)
    {
        global $API;
        $result = $API->dbh->query("select * from notes_products WHERE note_id='" . $id . "'");
        if ($result) {
            $products = $result->fetchAll(PDO::FETCH_ASSOC);
            if (count($products) > 0) {
                foreach ($products as &$product) {
                    $product["product_nome"] = urldecode($product["product_nome"]);
                    sp_utils_format_price($product["preco"]);
                }
                return $products;
            } else {
                return array();
            }
        }
        return array();
    }

    function generate_pdf($note_id)
    {
        global $API;
        $query = "select * from notes where id='" . $note_id . "'";
        $result = $API->dbh->query($query);
        if ($result) {
            $result = $result->fetch(PDO::FETCH_ASSOC);
            if ($result) {
                foreach ($result as &$val) $val = urldecode($val);
                $result["products"] = $this->get_note_products($note_id);
                $note = $result;
            } else {
                return false;
            }
        }

        $client = array();
        $result = $API->dbh->query("select * from clients WHERE id='" . $note["client_id"] . "'");
        if ($result) {
            $client = $result->fetch(PDO::FETCH_ASSOC);
            foreach ($client as &$val) $val = urldecode($val);
        }

        $payee_client = array();
        $result = $API->dbh->query("select * from clients WHERE id='" . $note["payee_id"] . "'");
        if ($result) {
            $payee_client = $result->fetch(PDO::FETCH_ASSOC);
            foreach ($payee_client as &$val) $val = urldecode($val);
        }

        $user = array();
        $result = $API->dbh->query("select * from users WHERE id='" . $note["user_id"] . "'");
        if ($result) $user = $result->fetch(PDO::FETCH_ASSOC);
        $users_to_notify = array();

        $result = $API->dbh->query("select * from configurations where user_id='" . $user["id"] . "' AND name='email_notification'");
        if ($result) {
            $result = $result->fetch(PDO::FETCH_ASSOC);
            if ($result)
                if ($result["value"] == "true")
                    $users_to_notify[] = array("type" => "seller", "email" => $user["email"], "nome" => $user["nome"], "id" => $user["id"]);
        }

        $send = true;
        $result = $API->dbh->query("select * from users where client_id='" . $client["id"] . "'");
        if ($result) {
            $result = $result->fetch(PDO::FETCH_ASSOC);
            if ($result) {
                $result = $API->dbh->query("select * from configurations where user_id='" . $result["id"] . "' AND name='email_notification'");
                if ($result) {
                    $result = $result->fetch(PDO::FETCH_ASSOC);
                    if ($result)
                        if ($result["value"] == "false") {
                            $send = false;
                        }
                }
            }
        }
        if ($send) {
            $users_to_notify[] = array("type" => "client", "email" => $client["email"], "nome" => $client["nome"], "id" => $client["id"]);
        }
        $result = $API->dbh->query("select * from users where tipo='0'");
        if ($result) {
            $users = $result->fetchAll(PDO::FETCH_ASSOC);
            if ($users) {
                foreach ($users as $user_mail) {
                    $send = true;
                    $result = $API->dbh->query("select * from configurations where user_id='" . $user_mail["id"] . "' AND name='email_notification'");
                    if ($result) {
                        $result = $result->fetch(PDO::FETCH_ASSOC);
                        if ($result)
                            if ($result["value"] == "false") {
                                $send = false;
                            }
                    }
                    if ($send) {
                        $users_to_notify[] = array("type" => "admin", "email" => $user_mail["email"], "nome" => $user_mail["nome"], "id" => $user_mail["id"]);
                    }
                }
            }
        }
        $result = $API->dbh->query("select nome from groups WHERE id='" . $user["grupo"] . "'");
        if ($result) $user["grupo_nome"] = end($result->fetch(PDO::FETCH_ASSOC));

        $log_options = array();
        $log_options["type"] = "note_new";
        $log_options["note_id"] = $note_id;
        $log_options["comment"] = $note["codigo"] . "|Encomenda";
        $log_options["client_id"] = $note["client_id"];
        $log_options["user_id"] = $note["user_id"];
        $log_options["status"] = 1;
        $API->log_action($log_options);
        if ($note["client_id"] != $note["payee_id"]) {
            $email_content = file_get_contents("includes/utils/email-templates/encomenda_pdf_dif.html");
        }
        else {
            $email_content = file_get_contents("includes/utils/email-templates/encomenda_pdf.html");
        }
        $replace = array();
        $replace["@VALOR_NOTA_URL@"] = "http://" . $_SERVER["HTTP_HOST"] . "/areacliente/#dashboard#modal#note#" . $note_id;
        $replace["@VALOR_NOTA_CODIGO@"] = $note["codigo"];
        $replace["@VALOR_NOTA_DATA_CRIACAO@"] = $note["data"];
        $replace["@VALOR_NOTA_COMENTARIO@"] = $note["comment"];
        $replace["@VALOR_NOTA_DATA_PAGAMENTO@"] = $note["data_payment"];


        $replace["@VALOR_NOTA_ESTADO@"] = $note["estado"];
        if ($replace["@VALOR_NOTA_ESTADO@"] == "0") {
            $replace["@VALOR_NOTA_ESTADO@"] = "Pendente";
        }
        if ($replace["@VALOR_NOTA_ESTADO@"] == "1") {
            $replace["@VALOR_NOTA_ESTADO@"] = "Em pagamento";
        }
        if ($replace["@VALOR_NOTA_ESTADO@"] == "2") {
            $replace["@VALOR_NOTA_ESTADO@"] = "Em progresso";
        }
        if ($replace["@VALOR_NOTA_ESTADO@"] == "3") {
            $replace["@VALOR_NOTA_ESTADO@"] = "Rejeitada";
        }
        if ($replace["@VALOR_NOTA_ESTADO@"] == "4") {
            $replace["@VALOR_NOTA_ESTADO@"] = "Fechada";
        }

        if ($note["client_id"] != $note["payee_id"]) {
            $replace["@VALOR_NOTA_DATA_PAGAMENTO@"] = "";
        }

        if ($replace["@VALOR_NOTA_DATA_PAGAMENTO@"] == "") {
            $email_content = sp_utils_delete_between("<!-- TO REMOVE PAGAMENTO START -->", "<!-- TO REMOVE PAGAMENTO END -->", $email_content);
        }

        $replace["@VALOR_NOTA_PRECO@"] = sp_utils_format_price($note["preco"]);
        $replace["@VALOR_VENDEDOR_URL@"] = "http://" . $_SERVER["HTTP_HOST"] . "/areacliente/#dashboard#modal#user#" . $note["user_id"];
        $replace["@VALOR_VENDEDOR_NOME@"] = $note["user_nome"];
        $replace["@VALOR_VENDEDOR_GRUPO@"] = $user["grupo_nome"];
        $replace["@VALOR_VENDEDOR_EMAIL@"] = $user["email"];

        $replace["@VALOR_CLIENTE_FINAL_URL@"] = "http://" . $_SERVER["HTTP_HOST"] . "/areacliente/#dashboard#modal#client#" . $note["client_id"];
        $replace["@VALOR_CLIENTE_FINAL_NOME@"] = $client["nome"];
        $replace["@VALOR_CLIENTE_FINAL_CODIGO@"] = $client["codigo"];
        $replace["@VALOR_CLIENTE_FINAL_EMAIL@"] = $client["email"];
        $replace["@VALOR_CLIENTE_FINAL_NIF@"] = $client["nif"];
        $replace["@VALOR_CLIENTE_FINAL_RESPONSAVEL@"] = $client["responsavel"];
        $replace["@VALOR_CLIENTE_FINAL_MORADA@"] = $client["morada"] . "<br>" . $client["cdpostal"] . " " . $client["local"];

        $replace["@VALOR_CLIENTE_FATURADO_URL@"] = "http://" . $_SERVER["HTTP_HOST"] . "/areacliente/#dashboard#modal#client#" . $note["payee_id"];
        $replace["@VALOR_CLIENTE_FATURADO_NOME@"] = $payee_client["nome"];
        $replace["@VALOR_CLIENTE_FATURADO_CODIGO@"] = $payee_client["codigo"];
        $replace["@VALOR_CLIENTE_FATURADO_EMAIL@"] = $payee_client["email"];
        $replace["@VALOR_CLIENTE_FATURADO_NIF@"] = $payee_client["nif"];
        $replace["@VALOR_CLIENTE_FATURADO_RESPONSAVEL@"] = $payee_client["responsavel"];
        $replace["@VALOR_CLIENTE_FATURADO_MORADA@"] = $payee_client["morada"] . "<br>" . $payee_client["cdpostal"] . " " . $payee_client["local"];

        $replace["@VALOR_PRODUTOS@"] = "";
        $replace["@VALOR_NOTA_PRECO_SEM_IVA@"] = 0;
        $replace["@VALOR_NOTA_TOTAL_IVA@"] = 0;
        $replace["@VALOR_NOTA_TOTAL_QUANTIDADE@"] = 0;
        $replace["@VALOR_NOTA_TOTAL_BONUS@"] = 0;
        $counter = 1;

        $td_name_width = "280";
        if ($note["client_id"] != $note["payee_id"]) {
            $td_name_width = "460";
        }

        foreach ($note["products"] as $produto) {
            $replace["@VALOR_PRODUTOS@"] .= '       <tr>';
            $replace["@VALOR_PRODUTOS@"] .= '       <td align="left" style="border:solid 1px #ccc;width:'.$td_name_width.'px;line-height:14px;"><font color="#777" size="7px">' . $counter . '. ' . (strlen($produto["codigo"]) > 7 ? "EAN" : "CNP") . ':</font> <a target="_blank" href="' . "http://" . $_SERVER["HTTP_HOST"] . "/areacliente/##modal#product#" . $produto["id"] . '#open" style="color:#000;">' . $produto["codigo"] . '</a> <font color="#777" size="7px">Nome:</font> ' . $produto["product_nome"] . '</td>';
            $replace["@VALOR_PRODUTOS@"] .= '       <td align="right" style="border:solid 1px #ccc;width:70px;line-height:14px;">' . $produto["quantidade"] . '</td>';
            $replace["@VALOR_PRODUTOS@"] .= '       <td align="right" style="border:solid 1px #ccc;width:48px;line-height:14px;">' . $produto["bonus"] . '</td>';
            $replace["@VALOR_PRODUTOS@"] .= '       <td align="right" style="border:solid 1px #ccc;width:60px;line-height:14px;">' . ($produto["bonus"] * 100 / $produto["quantidade"]) . '%</td>';

            if (!($note["client_id"] != $note["payee_id"])) {
                $replace["@VALOR_PRODUTOS@"] .= '       <td align="right" style="border:solid 1px #ccc;width:50px;line-height:14px;">' . $produto["preco"] . '€</td>';
                $replace["@VALOR_PRODUTOS@"] .= '       <td align="right" style="border:solid 1px #ccc;width:50px;line-height:14px;">' . $produto["iva"] . '%</td>';
                $preco = str_replace(",", ".", $produto["preco"]) * $produto["quantidade"];
                sp_utils_format_price($preco);
                $replace["@VALOR_PRODUTOS@"] .= '       <td align="right" style="border:solid 1px #ccc;width:80px;line-height:14px;">' . $preco . '€</td>';
            }
            $replace["@VALOR_PRODUTOS@"] .= '       </tr>';

            $replace["@VALOR_NOTA_PRECO_SEM_IVA@"] += str_replace(",", ".", $produto["preco"]) * $produto["quantidade"];
            $replace["@VALOR_NOTA_TOTAL_IVA@"] += str_replace(",", ".", $produto["preco"]) * $produto["quantidade"] * ($produto["iva"] / 100);

            $replace["@VALOR_NOTA_TOTAL_QUANTIDADE@"] += $produto["quantidade"];
            $replace["@VALOR_NOTA_TOTAL_BONUS@"] += $produto["bonus"];
            $counter++;
        }
        sp_utils_format_price($replace["@VALOR_NOTA_PRECO_SEM_IVA@"]);
        sp_utils_format_price($replace["@VALOR_NOTA_TOTAL_IVA@"]);
        $replace["@VALOR_ADMIN_URL@"] = "http://" . $_SERVER["HTTP_HOST"] . "/areacliente/#dashboard#modal#configuration#open";
        $replace["@VALOR_PLATFORM_URL@"] = "http://" . $_SERVER["HTTP_HOST"] . "/areacliente/";

        $type = pathinfo("includes/utils/email-templates/logo-bw.jpg", PATHINFO_EXTENSION);
        $data = file_get_contents("includes/utils/email-templates/logo-bw.jpg");
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

        $replace["@VALOR_LOGO@"] = $base64;

        $email_content = str_replace(array_keys($replace), array_values($replace), $email_content);

        if ($note["payee_id"] == $note["client_id"]) $email_content = sp_utils_delete_between("<!-- TO REMOVE FATURADO START -->", "<!-- TO REMOVE FATURADO END -->", $email_content);
        else $email_content = sp_utils_delete_between("<!-- TO REMOVE FINAL START -->", "<!-- TO REMOVE FINAL END -->", $email_content);

        require_once 'includes/utils/tcpdf/tcpdf.php';

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('SparMedix');
        $pdf->SetTitle('Encomenda ' . $note["codigo"]);
        $pdf->SetSubject('Detalhes da encomenda ' . $note["codigo"]);
        $pdf->SetKeywords('SparMedix, Encomenda, ' . $note["codigo"] . ', ' . $note["user_nome"] . ', ' . $client["nome"] . ', ' . $payee_client["nome"]);
        $pdf->SetMargins(15, 5, 15);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->AddPage();
        $pdf->writeHTML($email_content, true, false, true, false, '');
        $pdf->Output("database/orders/" . $note["codigo"] . '.pdf', 'F');

        return true;
    }
}

?>