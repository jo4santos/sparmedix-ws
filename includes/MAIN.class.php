<?php
if(!class_exists("MAIN")){
	class MAIN extends API
	{
	    public $user;
	
	    public function __construct($request, $origin) {
	        parent::__construct($request);	
			global $API;

			if(isset($this->request["chave"]) && (($this->endpoint == "mb") || ($this->endpoint == "clients") || ($this->endpoint == "products") || ($this->endpoint == "images"))) {
				if($this->endpoint == "mb" && $this->request["chave"] == 'Z.ZG9NgXK5T4CvnXsS0QNJJSX42ka/G') {
				 	
				}
				elseif(($this->endpoint == "clients" || $this->endpoint == "products" || $this->endpoint == "images") && $this->request["chave"] == '$2a$10$h1s2a3h4s5i6d7e8m9rapeAD/3MFLwneHfNiMrT.Kx9NhtZrbaxY.') {
				
				}
				else {
					throw new InvalidArgumentException('Acesso rejeitado');
				}
			}
            elseif($this->endpoint == "images") {

            }
			else if(!($this->endpoint=="auth" && $this->method=="post"))
			{
				$auth_token = "";
				if(sp_utils_cookies_get("auth-token"))
					$auth_token = sp_utils_cookies_get("auth-token");
				if(isset($this->request["auth_token"]))
					$auth_token = $this->request["auth_token"];
				//error_log("auth-token:".$auth_token);
				if(!$auth_token) throw new InvalidArgumentException('Utilizador sem sessao iniciada');
				$result = $this->dbh->query("select id,nome,client_id,email,grupo,tipo,session_start from users where hash='".$auth_token."'");
				if ( $result )	{
					$result = $result->fetch(PDO::FETCH_ASSOC);
					if ( !$result )	throw new UnexpectedValueException('Chave de autenticacao invalida');
					if ( time() - $result["session_start"] > 60/*s*/*60/*m*/*24/*h*/*7/*d*/ ) throw new OverflowException('Chave de autenticacao expirada');
					
					$this->user = array("id"=>$result["id"],"nome"=>$result["nome"],"session_start"=>$result["session_start"],"client_id"=>$result["client_id"],"email"=>$result["email"],"tipo"=>$result["tipo"],"grupo"=>$result["grupo"],"clientes"=>array(),"produtos"=>array());
					
					if($this->user["grupo"])
					{
						$result = $this->dbh->query("select * from groups_clients WHERE group_id='".$this->user["grupo"]."'");
						if ( $result ) {
							$clients = $result->fetchAll(PDO::FETCH_ASSOC);
							if( count ( $clients ) > 0 ) {
								$this->user["clientes"] = array();
								foreach ( $clients as $client ) {
									$this->user["clientes"][] = $client["client_id"];
								}
							}
						}
						if($this->user["tipo"]=="2") $this->user["clientes"]=array($this->user["client_id"]);
						$result = $this->dbh->query("select * from groups_products WHERE group_id='".$this->user["grupo"]."'");
						if ( $result ) {
							$products = $result->fetchAll(PDO::FETCH_ASSOC);
							if( count ( $products ) > 0 ) {
								$this->user["produtos"] = array();
								foreach ( $products as $product ) {
									$this->user["produtos"][] = $product["product_id"];
								}
							}
						}			
					}
				}
			}
	    }
	    
		protected function groups() {
			include "groups.class.php";
			$obj = new groups($_REQUEST['_url']);
			return $obj->process();
		}
	    
		protected function users() {
			include "users.class.php";
			$obj = new users($_REQUEST['_url']);
			return $obj->process();
		}
	    
		protected function auth() {
			include "auth.class.php";
			$obj = new auth($_REQUEST['_url']);
			return $obj->process();
		}
	    
		protected function clients() {
			include "clients.class.php";
			$obj = new clients($_REQUEST['_url']);
			return $obj->process();
		}
	    
		protected function products() {
			include "products.class.php";
			$obj = new products($_REQUEST['_url']);
			return $obj->process();
		}
	    
		protected function notes() {
			include "notes.class.php";
			$obj = new notes($_REQUEST['_url']);
			return $obj->process();
		}
	    
		protected function mb() {
			include "mb.class.php";
			$obj = new mb($_REQUEST['_url']);
			return $obj->process();
		}
	    
		protected function images() {
			include "images.class.php";
			$obj = new images($_REQUEST['_url']);
			return $obj->process();
		}
	    
		protected function apk() {
			include "apk.class.php";
			$obj = new apk($_REQUEST['_url']);
			return $obj->process();
		}

		protected function notifications() {
			global $API;

			if($this->endpoint == "notifications") {
				$notifications = $this->dbh->query("select * from actions WHERE user_id='".$this->user["id"]."' OR client_id='".$this->user["client_id"]."'")->fetchAll(PDO::FETCH_ASSOC);
				$notifications = array_reverse($notifications);
				return $API->_response($notifications);
				echo "<table border='1' cellpadding='10'>";
				foreach($notifications as $notification) {

					echo "<tr>";
					echo "<td>".$notification["id"]."</td>";
					echo "<td>".$notification["type"]."</td>";
					echo "<td>".$notification["user_id"]."</td>";
					echo "<td>".$notification["group_id"]."</td>";
					echo "<td>".$notification["product_id"]."</td>";
					echo "<td>".$notification["client_id"]."</td>";
					echo "<td>".$notification["note_id"]."</td>";
					echo "<td>".$notification["status"]."</td>";
					echo "<td>".$notification["comment"]."</td>";
					echo "<td>".$notification["data"]."</td>";
					echo "</tr>";
				}
				echo "</table>";
				throw new LogicException();
			}
		}
		
		public function test() {
			echo "TESTE";
		}
	}
}
 ?>