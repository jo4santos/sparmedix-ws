<?php
if(!class_exists("notes")){
	class notes
	{
	    public function __construct() {
			
	    }
	    
	    public function process() {
			global $API;
        	if ((int)method_exists($this, $API->method) > 0) {
        		return $this->{$API->method}($API->args);
        		if($retval!=false)
					return $API->_response($retval);
        		else
        			return $API->_response("Internal error: $API->method", 500);
        	}
        	return $API->_response("No method: $API->method", 405);
	    }
	    
		protected function put() {
			global $API;

			$notes = array();
			if(isset($API->put_contents["notes"]))
				$notes = $API->put_contents["notes"];

			// TODO: Verify and return error per note
			// TODO: Check user_id
			
			//error_log(print_r($notes,true));
			//return $API->_response("Erro temp ao inserir produto da nota", 500);

			if(isset($API->args[0])) {
				if($API->args[0]=="reject") {
					$id = "";
					if(isset($API->put_contents["id"]))
						$id = $API->put_contents["id"];

					if($id != "") {
						try{
							$estado = 3;
							$stmt = $API->dbh->prepare("UPDATE notes SET estado=?,reject_user_id=?,reject_user_name=?,reject_data=?,referencia='' WHERE id='$id'");
							$stmt->bindParam(1, $estado);
							$stmt->bindParam(2, $API->user["id"]);
							$stmt->bindParam(3, $API->user["nome"]);
							$stmt->bindParam(4, date('Y-m-d H:i:s',time()));
							$stmt->execute();
						}
						catch(PDOException $e) {
							return $API->_response("Database error in edit", 500);
						}

						$API->generate_pdf($id);

						$this->send_order_email($id,true,$estado);

						$note = $API->dbh->query("SELECT * FROM notes WHERE id='$id'")->fetch(PDO::FETCH_ASSOC);
						$log_options = array();
						$log_options["type"] = "note_cancelled";
						$log_options["note_id"] = $id;
						$log_options["comment"] = $note["codigo"]."|".$note["reject_user_id"]."|".$note["reject_user_name"];
						$log_options["client_id"] = $note["client_id"];
						$log_options["user_id"] = $note["user_id"];
						$log_options["status"] = 1;
						$API->log_action($log_options);
						return $API->_response($id);
					}
				}
				elseif($API->args[0]=="close") {
					$id = "";
					if(isset($API->put_contents["id"]))
						$id = $API->put_contents["id"];

					if($id != "") {
						try{
							$estado = 4;
							$stmt = $API->dbh->prepare("UPDATE notes SET estado=?,close_user_id=?,close_user_name=?,close_data=? WHERE id='$id'");
							$stmt->bindParam(1, $estado);
							$stmt->bindParam(2, $API->user["id"]);
							$stmt->bindParam(3, $API->user["nome"]);
							$stmt->bindParam(4, date('Y-m-d H:i:s',time()));
							$stmt->execute();
						}
						catch(PDOException $e) {
							return $API->_response("Database error in edit", 500);
						}
						$API->generate_pdf($id);
						//$this->send_order_email($id,true,$estado);
						$note = $API->dbh->query("SELECT * FROM notes WHERE id='$id'")->fetch(PDO::FETCH_ASSOC);
						$log_options = array();
						$log_options["type"] = "note_closed";
						$log_options["note_id"] = $id;
						$log_options["comment"] = $note["codigo"]."|".$note["close_user_id"]."|".$note["close_user_name"];
						$log_options["client_id"] = $note["client_id"];
						$log_options["user_id"] = $note["user_id"];
						$log_options["status"] = 1;
						$API->log_action($log_options);
						return $API->_response($id);
					}
				}
			}

			$note_ids = array();


			foreach($notes as &$note) {
				sp_utils_format_price($note["preco"]);
				
				//			note.codigo = sp.utils.get_initials(note.user_nome)+sp.utils.zerofill(note.user_id,3)+year+sp.utils.zerofill(sp.db.notes.last_id,4)+"T";
				
				$initials = $user_id = $year = $note_id = "";
				foreach(explode(" ",$API->user["nome"]) as $w){$initials .= strtoupper($w[0]);}
				$user_id = sprintf("%03d",$API->user["id"]);
				$year = date("Y");				
				$note_id = sprintf("%04d",count($API->dbh->query("SELECT * FROM notes WHERE user_id='".$API->user["id"]."'")->fetchAll())+1);

				$note["codigo"] = $initials.$user_id.$year.$note_id;

				try{
					$stmt = $API->dbh->prepare("INSERT INTO notes (client_id,payee_id,user_id,codigo,referencia,data,estado,preco,client_nome,payee_nome,user_nome,data_payment,comment) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
					$stmt->bindParam(1, $note["client_id"]);
					$stmt->bindParam(2, $note["payee_id"]);
					$stmt->bindParam(3, $note["user_id"]);
					$stmt->bindParam(4, $note["codigo"]);
					$stmt->bindParam(5, $note["referencia"]);
					$stmt->bindParam(6, $note["data"]);
					$stmt->bindParam(7, $note["estado"]);
					$stmt->bindParam(8, $note["preco"]);
					$stmt->bindParam(9, urlencode($note["client_nome"]));
					$stmt->bindParam(10, urlencode($note["payee_nome"]));
					$stmt->bindParam(11, $note["user_nome"]);
					$data_payment = "";
					if($note["client_id"]!=$note["payee_id"] && $note["payee_id"])
						$data_payment = $note["data"];
					$stmt->bindParam(12, $data_payment);
					$stmt->bindParam(13, $note["comment"]);
					$stmt->execute();

					$query = "INSERT INTO notes (client_id,payee_id,user_id,codigo,referencia,data,estado,preco,client_nome,payee_nome,user_nome,data_payment) VALUES (";
					$query .= "'".$note["client_id"]."'";
					$query .= ",'".$note["payee_id"]."'";
					$query .= ",'".$note["user_id"]."'";
					$query .= ",'".$note["codigo"]."'";
					$query .= ",'".$note["referencia"]."'";
					$query .= ",'".$note["data"]."'";
					$query .= ",'".$note["estado"]."'";
					$query .= ",'".$note["preco"]."'";
					$query .= ",'".urlencode($note["client_nome"])."'";
					$query .= ",'".urlencode($note["payee_nome"])."'";
					$query .= ",'".$note["user_nome"]."'";
					$query .= ",'".$data_payment."'";
					$query .= ",'".$note["comment"]."'";
					$query .= ")";
					//error_log($query);
				}
				catch(Exception $e) {
					error_log("Erro de BD ao inserir nota");
					return $API->_response("Erro de BD ao inserir nota", 500);
				}
				$inserted_note_id = $API->dbh->lastInsertId();

				$note_ids[] = array("id"=>$inserted_note_id,"codigo"=>$note["codigo"]);

				foreach($note["products"] as &$product) {
					sp_utils_format_price($product["preco"]);
					try{
						$stmt = $API->dbh->prepare("INSERT INTO notes_products (note_id,product_id,quantidade,bonus,preco,iva,product_nome,codigo) VALUES (?,?,?,?,?,?,?,?)");
						$stmt->bindParam(1, $inserted_note_id);
						$stmt->bindParam(2, $product["id"]);
						$stmt->bindParam(3, $product["quantidade"]);
						$stmt->bindParam(4, $product["bonus"]);
						$stmt->bindParam(5, $product["preco"]);
						$stmt->bindParam(6, $product["iva"]);
						$stmt->bindParam(7, urlencode($product["nome"]));
						$stmt->bindParam(8, $product["codigo"]);
						$stmt->execute();
					}
					catch(PDOException $e) {
						return $API->_response("Erro de BD ao inserir produto da nota", 500);
					}
				}
				$this->send_order_email($inserted_note_id);
			}

			return $API->_response($note_ids);
		}
	    
		protected function get() {
			global $API;
			
// 			try{
// 				$stmt = $API->dbh->prepare("INSERT INTO notes (client_id,payee_id,user_id,codigo,referencia,data,estado,preco) VALUES (?,?,?,?,?,?,?,?)");
// 				$stmt->bindParam(1, $a = '1');
// 				$stmt->bindParam(2, $b = '1');
// 				$stmt->bindParam(3, $c = '3');
// 				$stmt->bindParam(4, $d = '1231234');
// 				$stmt->bindParam(5, $e = '123123123');
// 				$stmt->bindParam(6, $f = date('Y-m-d H:i:s',time()));
// 				$stmt->bindParam(7, $g = '0');
// 				$stmt->bindParam(8, $h = '123123,34');
// 				$stmt->execute();
				
// 			}
// 			catch(PDOException $e) {
// 				return $API->_response("Erro de BD ao inserir nota", 500);
// 			}
			
// 			return $API->_response("Erro ao obter informacoes das notas de encomenda", 500);
			/*try{
  	 			$stmt = $API->dbh->exec("UPDATE products SET (familia) VALUES ('teste');");
  	 			return $API->_response("OK");
			}
 		 	catch(PDOException $e) {
 		 		return $API->_response("NOK", 500);
 		 	}*/


 		 	/*try{
  	 			$stmt = $API->dbh->exec("ALTER TABLE notes ADD COLUMN comment VARCHAR(512) DEFAULT '';");
  	 			return $API->_response("OK");
			}
 		 	catch(PDOException $e) {
 		 		return $API->_response("NOK", 500);
 		 	}
 		 	return $API->_response("Adicionada coluna", 500);*/
					
			if(isset($API->args[0])) {
				if($API->args[0] == "mail") {
					$this->send_order_email($API->args[1]);
					exit();
				}
				if($API->args[0] == "pdf") {
					if ( !is_numeric ( $API->args[1] ) ) return $API->_response("Id de nota invalido ".$API->args[1], 400);
					$query = "select * from notes where id='".$API->args[1]."'";
					if($API->user["tipo"]=="2") {
						$query .= " and client_id='".$API->user["client_id"]."'";
					}
					if($API->user["tipo"]=="1") {
						$query .= " and user_id='".$API->user["user_id"]."'";
					}				
					$result = $API->dbh->query($query);

					if ( $result )	{
						$result = $result->fetch(PDO::FETCH_ASSOC);
						if ( $result )
						{
							$file =  realpath(dirname($_SERVER['SCRIPT_FILENAME']))."/../database/orders/".$result["codigo"].'.pdf';
							$filename = $result["codigo"].'.pdf'; /* Note: Always use .pdf at the end. */

							if(isset($API->args[2])) {
								if($API->args[2]=="download") {

									header('Content-type: application/pdf');
									header('Content-Disposition: attachment; filename="' . $filename . '"');
									header('Content-Transfer-Encoding: binary');
									header('Content-Length: ' . filesize($file));
									header('Accept-Ranges: bytes');

									@readfile($file);
									return true;
								}
							}

							header('Content-type: application/pdf');
							header('Content-Disposition: inline; filename="' . $filename . '"');
							header('Content-Transfer-Encoding: binary');
							header('Content-Length: ' . filesize($file));
							header('Accept-Ranges: bytes');

							@readfile($file);
							return true;
						}
						else {
							return $API->_response("Nota nao encontrada: id ".$API->args[1], 404);
						}
					}
					exit();
				}
			}

			if ( count ( $API->args ) > 0 ) {
				if ( !is_numeric ( $API->args[0] ) ) return $API->_response("Id de nota invalido ".$API->args[0], 400);
				$query = "select * from notes where id='".$API->args[0]."'";
				if($API->user["tipo"]!="2") {
					$query .= " and client_id='".$API->user["client_id"]."'";
				}
				if($API->user["tipo"]!="1") {
					$query .= " and user_id='".$API->user["user_id"]."'";
				}				
				$result = $API->dbh->query($query);
				if ( $result )	{
					$result = $result->fetch(PDO::FETCH_ASSOC);
					if ( $result )
					{
						$result["client_nome"] = urldecode($result["client_nome"]);
						$result["payee_nome"] = urldecode($result["payee_nome"]);
						sp_utils_format_price($result["preco"]);
						$result["produtos"] = $API->get_note_products($API->args[0]);
						return $API->_response($result);
					}
					else {
						return $API->_response("Nota nao encontrada: id ".$API->args[0], 404);
					}
				}
			}
			else {
				$query = "select * from notes";
				if($API->user["tipo"]=="2") {
					$query .= " where client_id='".$API->user["client_id"]."'";
				}
				if($API->user["tipo"]=="1") {
					$query .= " where user_id='".$API->user["id"]."'";
				}
				$query .= " order by data DESC";
				$result = $API->dbh->query($query);
				if ( $result ) {
					$elements = $result->fetchAll(PDO::FETCH_ASSOC);
					if( count ( $elements ) > 0 ) {
						foreach ( $elements as &$element )
						{
							$element["payee_nome"] = urldecode($element["payee_nome"]);
							$element["client_nome"] = urldecode($element["client_nome"]);
							sp_utils_format_price($element["preco"]);
							$element["produtos"] = $API->get_note_products($element["id"]);
						}
						return $API->_response($elements);
					}
					else {
						return $API->_response(array());
					}
				}
			}			
			
			return $API->_response("Erro ao obter informacoes das notas de encomenda", 500);
		}
		
		protected function post() {
			global $API;

			return $API->_response("No method: $API->method", 405);
		}
		
		protected function delete() {
			global $API;

			return $API->_response("No method: $API->method", 405);
		}
		protected function send_order_email($note_id,$update = false,$next=false) {
			global $API;

			$query = "select * from notes where id='".$note_id."'";
			$result = $API->dbh->query($query);
			if ( $result )	{
				$result = $result->fetch(PDO::FETCH_ASSOC);
				if ( $result )
				{
					$result["client_nome"] = urldecode($result["client_nome"]);
					$result["payee_nome"] = urldecode($result["payee_nome"]);
					$result["products"] = $API->get_note_products($note_id);
					$note = $result;
				}
				else {
					return false;
				}
			}

			$client = array();
			$result = $API->dbh->query("select * from clients WHERE id='".$note["client_id"]."'");
			if ( $result ) {
				$client = $result->fetch(PDO::FETCH_ASSOC);
				foreach ($client as &$val) $val = urldecode($val);
			}
			$payee_client = array();
			$result = $API->dbh->query("select * from clients WHERE id='".$note["payee_id"]."'");
			if ( $result ) {
				$payee_client = $result->fetch(PDO::FETCH_ASSOC);
				foreach ($payee_client as &$val) $val = urldecode($val);
			}
			
			$user = array();
			$result = $API->dbh->query("select * from users WHERE id='".$note["user_id"]."'");
			if ( $result ) $user = $result->fetch(PDO::FETCH_ASSOC);
			$users_to_notify = array();
		
			$result = $API->dbh->query("select * from configurations where user_id='".$user["id"]."' AND name='email_notification'");
			if ( $result )	{
				$result = $result->fetch(PDO::FETCH_ASSOC);
				if ( $result )
					if($result["value"]=="true")
						$users_to_notify[] = array("type"=>"seller","email"=>$user["email"],"nome"=>$user["nome"],"id"=>$user["id"]);
			}
			
			$send = true;
			$result = $API->dbh->query("select * from users where client_id='".$client["id"]."'");
			if ( $result )	{
				$result = $result->fetch(PDO::FETCH_ASSOC);
				if ( $result ) {
					$result = $API->dbh->query("select * from configurations where user_id='".$result["id"]."' AND name='email_notification'");
					if ( $result )	{
						$result = $result->fetch(PDO::FETCH_ASSOC);
						if ( $result )
							if($result["value"]=="false") {
								$send = false;
							}
					}
				}
			}
			if($send) {
				$users_to_notify[] = array("type"=>"client","email"=>$client["email"],"nome"=>$client["nome"],"id"=>$client["id"]);
			}
			$result = $API->dbh->query("select * from users where tipo='0'");
			if ( $result )	{
				$users = $result->fetchAll(PDO::FETCH_ASSOC);
				if ( $users ) {
					foreach ($users as $user_mail) {
						$send = true;
						$result = $API->dbh->query("select * from configurations where user_id='".$user_mail["id"]."' AND name='email_notification'");
						if ( $result )	{
							$result = $result->fetch(PDO::FETCH_ASSOC);
							if ( $result )
								if($result["value"]=="false") {
									$send = false;
								}
						}
						if($send) {
							$users_to_notify[] = array("type"=>"admin","email"=>$user_mail["email"],"nome"=>$user_mail["nome"],"id"=>$user_mail["id"]);
						}
					}
				}
			}

            $lista_distribuicao = "";
			$result = $API->dbh->query("select nome,lista_distribuicao from groups WHERE id='".$user["grupo"]."'");
			if ( $result ) {
                $result = $result->fetchAll(PDO::FETCH_ASSOC);
                if(is_array($result)) $result = $result[0];
                $user["grupo_nome"] = $result["nome"];
                $lista_distribuicao = explode(";",$result["lista_distribuicao"]);
            }

			$log_options = array();
			$log_options["type"] = "note_new";
			$log_options["note_id"] = $note_id;
			$log_options["comment"] = $note["codigo"]."|Encomenda";
			$log_options["client_id"] = $note["client_id"];
			$log_options["user_id"] = $note["user_id"];
			$log_options["status"] = 1;
			$API->log_action($log_options);
            if ($note["client_id"] != $note["payee_id"]) {
                $email_content = file_get_contents("includes/utils/email-templates/encomenda_email_dif.html");
            }
            else {
                $email_content = file_get_contents("includes/utils/email-templates/encomenda_email.html");
            }
			$email_content = mb_convert_encoding($email_content, 'HTML-ENTITIES', "UTF-8");
			$replace = array();
			$replace["@VALOR_NOTA_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/#dashboard#modal#note#".$note_id;
			$replace["@VALOR_NOTA_CODIGO@"] = $note["codigo"];
			$replace["@VALOR_NOTA_DATA_CRIACAO@"] = $note["data"];
			$replace["@VALOR_NOTA_COMENTARIO@"] = $note["comment"];
			$replace["@VALOR_NOTA_PRECO@"] = sp_utils_format_price($note["preco"]);
			$replace["@VALOR_VENDEDOR_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/#dashboard#modal#user#".$note["user_id"];
			$replace["@VALOR_VENDEDOR_NOME@"] = $note["user_nome"];
			$replace["@VALOR_VENDEDOR_GRUPO@"] = $user["grupo_nome"];
			$replace["@VALOR_VENDEDOR_EMAIL@"] = $user["email"];
			
			$replace["@VALOR_CLIENTE_FINAL_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/#dashboard#modal#client#".$note["client_id"];
			$replace["@VALOR_CLIENTE_FINAL_NOME@"] = $client["nome"];
			$replace["@VALOR_CLIENTE_FINAL_CODIGO@"] = $client["codigo"];
			$replace["@VALOR_CLIENTE_FINAL_EMAIL@"] = $client["email"];
			$replace["@VALOR_CLIENTE_FINAL_NIF@"] = $client["nif"];
			$replace["@VALOR_CLIENTE_FINAL_RESPONSAVEL@"] = $client["responsavel"];
            $replace["@VALOR_CLIENTE_FINAL_MORADA@"] = $client["morada"]."<br>".$client["cdpostal"]." ".$client["local"];
			
			$replace["@VALOR_CLIENTE_FATURADO_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/#dashboard#modal#client#".$note["payee_id"];
			$replace["@VALOR_CLIENTE_FATURADO_NOME@"] = $payee_client["nome"];
			$replace["@VALOR_CLIENTE_FATURADO_CODIGO@"] = $payee_client["codigo"];
			$replace["@VALOR_CLIENTE_FATURADO_EMAIL@"] = $payee_client["email"];
			$replace["@VALOR_CLIENTE_FATURADO_NIF@"] = $payee_client["nif"];
			$replace["@VALOR_CLIENTE_FATURADO_RESPONSAVEL@"] = $payee_client["responsavel"];
            $replace["@VALOR_CLIENTE_FATURADO_MORADA@"] = $payee_client["morada"]."<br>".$payee_client["cdpostal"]." ".$payee_client["local"];
			
			$replace["@VALOR_PRODUTOS@"] = "";
			$replace["@VALOR_NOTA_PRECO_SEM_IVA@"] = 0;
			$replace["@VALOR_NOTA_TOTAL_IVA@"] = 0;
			foreach($note["products"] as $produto) {
				$replace["@VALOR_PRODUTOS@"] .= '		<tr>';
				$replace["@VALOR_PRODUTOS@"] .= '		<td align="left" style="border:solid 1px #ccc;line-height:14px;"><span style="color:#777;font-size:10px;">'.(strlen($produto["codigo"])>7?"EAN":"CNP").':</span>&nbsp;&nbsp; <a href="'."http://".$_SERVER["HTTP_HOST"]."/areacliente/##modal#product#".$produto["id"].'#open" style="color:#008dd2;text-decoration:underline;">'.$produto["codigo"].'</a><br /><span style="color:#777;font-size:10px;">Nome:</span> '.$produto["product_nome"].'</td>';
				$replace["@VALOR_PRODUTOS@"] .= '		<td align="right" style="border:solid 1px #ccc;line-height:28px;" nowrap>'.$produto["quantidade"].($produto["bonus"]>0?"+".$produto["bonus"]:"").'</td>';
                if (!($note["client_id"] != $note["payee_id"])) {
                    $replace["@VALOR_PRODUTOS@"] .= '		<td align="right" style="border:solid 1px #ccc;line-height:28px;" nowrap>' . $produto["preco"] . '&euro;</td>';
                    $replace["@VALOR_PRODUTOS@"] .= '		<td align="right" style="border:solid 1px #ccc;line-height:28px;" nowrap>' . $produto["iva"] . '%</td>';
                    $preco = $produto["preco"] * $produto["quantidade"];
                    sp_utils_format_price($preco);
                    $replace["@VALOR_PRODUTOS@"] .= '		<td align="right" style="border:solid 1px #ccc;width:90px;line-height:28px;" nowrap>' . $preco . '&euro;</td>';
                }
				$replace["@VALOR_PRODUTOS@"] .= '		</tr>';

				$replace["@VALOR_NOTA_PRECO_SEM_IVA@"] += $produto["preco"]*$produto["quantidade"];
				$replace["@VALOR_NOTA_TOTAL_IVA@"] += $produto["preco"]*$produto["quantidade"]*($produto["iva"]/100);
			}
			sp_utils_format_price($replace["@VALOR_NOTA_PRECO_SEM_IVA@"]);
			sp_utils_format_price($replace["@VALOR_NOTA_TOTAL_IVA@"]);
			$replace["@VALOR_ADMIN_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/#dashboard#modal#configuration#open";
			$replace["@VALOR_PLATFORM_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/";

			$type = pathinfo("includes/utils/email-templates/logo.png", PATHINFO_EXTENSION);
			$data = file_get_contents("includes/utils/email-templates/logo.png");
			$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

			$replace["@VALOR_LOGO@"] = $base64;

			if($update) {
				if($next==1) {
					$next = "Em pagamento";
					$next_cor = "#f0ad4e";
					$subject = "[ENC][".$note["codigo"]."][NOVA]".$note["client_nome"];
					$replace["@VALOR_NOTA_ESTADO_DESCRICAO@"] = "Dados para pagamento<br>Entidade: 11202<br>Referência: ".$note["referencia"]."<br>Valor: ".$replace["@VALOR_NOTA_PRECO@"];
				}
				else if($next==2) {
					$next = "Pago, em progresso";
					$next_cor = "#428bca";
				}
				else if($next==3) {
					$next = "Rejeitada";
					$next_cor = "#d9534f";
					$subject = "[ENC][".$note["codigo"]."][REJEITADA]".$note["client_nome"];
					$reject_role = " (Administrador)";
					if($note["reject_user_id"] == $note["client_id"]) $reject_role = " (Cliente)";
					if($note["reject_user_id"] == $note["user_id"]) $reject_role = " (Vendedor)";
					$replace["@VALOR_NOTA_ESTADO_DESCRICAO@"] = "Em <b>".$note["reject_data"]."</b> por <b>".$note["reject_user_name"]."</b>".$reject_role;
				}
				else if($next==4) {
					$next = "Tratada";
					$next_cor = "#5cb85c";
					$subject = "[ENC][".$note["codigo"]."][TRATADA]".$note["client_nome"];
					$close_role = " (Administrador)";
					if($note["close_user_id"] == $note["client_id"]) $close_role = " (Cliente)";
					if($note["close_user_id"] == $note["user_id"]) $close_role = " (Vendedor)";
					$replace["@VALOR_NOTA_ESTADO_DESCRICAO@"] = "Em <b>".$note["close_data"]."</b> por <b>".$note["close_user_name"]."</b>".$close_role;
				}
				$replace["@VALOR_NOTA_ESTADO_PROXIMO@"] = $next;
				$replace["@VALOR_NOTA_ESTADO_PROXIMO_COR@"] = $next_cor;
			}
			else {
				$subject = "[ENC][".$note["codigo"]."][NOVA]".$note["client_nome"];
				$replace["@VALOR_NOTA_ESTADO_PROXIMO@"] = "Em pagamento";
				$replace["@VALOR_NOTA_ESTADO_PROXIMO_COR@"] = "#f0ad4e";
				$replace["@VALOR_NOTA_ESTADO_DESCRICAO@"] = "Dados para pagamento<br> - Entidade: 11202<br> - Referencia: ".$note["referencia"]."<br> - Valor: ".$replace["@VALOR_NOTA_PRECO@"]."&euro;";

			}

			$email_content = str_replace(array_keys($replace), array_values($replace), $email_content);

			if($note["payee_id"] == $note["client_id"]) $email_content = sp_utils_delete_between("<!-- TO REMOVE FATURADO START -->","<!-- TO REMOVE FATURADO END -->",$email_content);
			else $email_content = sp_utils_delete_between("<!-- TO REMOVE FINAL START -->","<!-- TO REMOVE FINAL END -->",$email_content);


			$API->generate_pdf($note_id);

			$files = array();
			$files[] = array("path"=>realpath(dirname($_SERVER['SCRIPT_FILENAME']))."/../database/orders/".$note["codigo"].'.pdf',"name"=>$note["codigo"].'.pdf');

			$images = array();
			$images[] = array("path"=>realpath(dirname($_SERVER['SCRIPT_FILENAME']))."/../includes/utils/email-templates/logo.png","name"=>'logo.png',"cid"=>'logo.png');

			$email_to_send = array();
            foreach ($users_to_notify as $user) {
                if(!in_array($user["email"], $email_to_send )) {
                    $email_to_send[] = $user["email"];
                    $log_options = array();
                    $log_options["type"] = "note_new_email";
                    $log_options["note_id"] = $note_id;
                    $log_options["comment"] = $note["codigo"]."|".$user["email"]."|Encomenda";
                    if($user["type"]=="client") {
                        $log_options["client_id"] = $user["id"];
                    } elseif($user["type"]=="seller") {
                        $log_options["user_id"] = $user["id"];
                    } else {
                        $log_options["user_id"] = $user["id"];
                    }
                    if(sp_send_email($user["email"],"web@sparmedix.pt","Web Sparmedix", $subject, $email_content, $files, $images)) {
                        $log_options["status"] = 1;
                    } else {
                        $log_options["status"] = 0;
                    }
                    $API->log_action($log_options);
                }
            }

            foreach ($lista_distribuicao as $email) {
                if(!in_array($email, $email_to_send )) {
                    $email_to_send[] = $email;
                    sp_send_email($email,"web@sparmedix.pt","Web Sparmedix", $subject, $email_content, $files, $images);
                }
            }

		}
	}
}
?>