<?php
if(!class_exists("apk")){
	class apk
	{
	    public function __construct() {
			
	    }
	    
	    public function process() {
			global $API;
        	if ((int)method_exists($this, $API->method) > 0) {
        		return $this->{$API->method}($API->args);
        		if($retval!=false)
					return $API->_response($retval);
        		else
        			return $API->_response("Internal error: $API->method", 500);
        	}
        	return $API->_response("No method: $API->method", 405);
	    }
		
		protected function get() {
			global $API;

			$uploaddir = dirname(__FILE__)."/../../apk/release/";

			if ($handle = opendir($uploaddir)) {
        		while (false !== ($filename = readdir($handle))) {
        			$extension = strtolower(end(explode(".",$filename)));
            		if ($extension == "apk") {
            			$file = $uploaddir.$filename;

						header('Content-type: application/vnd.android.package-archive');
						header('Content-Disposition: attachment; filename="' . $filename . '"');
						header('Content-Transfer-Encoding: binary');
						header('Content-Length: ' . filesize($file));
						header('Accept-Ranges: bytes');

						@readfile($file);
        				closedir($handle);
						return true;
            		}
        		}
        		closedir($handle);
   			}
			return $API->_response("");
		}
	}
}
?>