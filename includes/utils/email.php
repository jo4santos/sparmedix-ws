<?php

if(!function_exists("sp_send_email"))
{
	function sp_send_email($destination_email, $origin_email, $origin_name, $subject, $content, $files = array(), $images = array())
	{
		require_once 'includes/utils/PHPMailer-5.2.7/PHPMailerAutoload.php';
		
		$subject = '=?utf-8?B?'.base64_encode($subject).'?=';

		$email = new PHPMailer();
		$email->From      = $origin_email;
		$email->FromName  = $origin_name;
		$email->Subject   = $subject;
		$email->Body      = $content;
		$email->IsHTML(true);
		$email->AddAddress( $destination_email );

		foreach ($files as $file) {
			if(file_exists($file["path"])) {
				$email->AddAttachment( $file["path"] , $file["name"] );
			}
		}

		foreach ($images as $image) {
			if(file_exists($image["path"])) {
				$email->AddEmbeddedImage( $image["path"] , $image["cid"] , $image["name"] );
			}
		}

		return $email->Send();

		/*
		if(JS_MAIL_SEND_TYPE == "swift")
		{
			$content .= JS_MAIL_CONTENT_SUFFIX;
			
			$transport = Swift_SmtpTransport::newInstance('mail.josesantos.eu', 25)
				->setUsername('js@josesantos.eu')
				->setPassword('qwe123ASD$%&');
			
			$mailer = Swift_Mailer::newInstance($transport);
			
			$message = Swift_Message::newInstance()
			->setSubject(JS_MAIL_SUBJECT_PREFIX.$subject)
			->setFrom(array($origin_email => $origin_name))
			->setTo(array($destination_email))
			->setBody(strip_tags($content))
			->addPart($content, 'text/html')
			;
			
			if ($mailer->send($message))
			{
				error_log("Message sent!");
				return true;
			}
			else
			{
				error_log("Message delivery failed...");
				return false;
			}
		}
		else
		{
			*/
			// validate all fields. Check for email headers! BCC for example. Empty strings
			
			$headers = "From: " . strip_tags($origin_email) . "\r\n";
			$headers .= "Reply-To: ". strip_tags($origin_email) . "\r\n";
			//$headers .= "CC: susan@example.com\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";		
			$headers .= "X-Mailer: php\r\n";
			
			$message = $content;
			//."<br>\n<br>\n---<br>\nPor favor não responda a este e-mail, foi automaticamente gerado pela aplicação Sparmedix. Para mais informações envie um e-mail para webmaster@sparmedix.pt";
			
		 	if (mail($destination_email, $subject, $message, $headers))
		 	{
				return true;
			} 
			else 
			{
				return false;
			}
		//}
	}
}
?>