<?php
if(!function_exists("sp_utils_cookies_set"))
{
	function sp_utils_cookies_set ($name, $value, $duration_minutes, $path = "/",$domain='',$secure=false,$httponly=false) {
		return setcookie(md5("sp-cookie-".$name), $value, time()+$duration_minutes*60, $path, $domain, $secure, $httponly);
	}
}
if(!function_exists("sp_utils_cookies_get"))
{
	function sp_utils_cookies_get ($name) {
		if(isset($_COOKIE[md5("sp-cookie-".$name)]))
		{
			return $_COOKIE[md5("sp-cookie-".$name)];
		}
		else
		{
			return false;
		}
	}
}
if(!function_exists("sp_utils_cookies_remove"))
{
	function sp_utils_cookies_remove ($name, $path = "/") {
		return sp_utils_cookies_set ($name, "", time()-3600, $path);
	}
}
?>