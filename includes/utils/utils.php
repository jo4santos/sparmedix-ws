<?php 
	include "cookies.php";
	include "email.php";

	function sp_utils_replace_in_string($string,$array) {
		print_r(array_keys($array));
		print_r(array_values($array));
	}
	function sp_utils_format_price(&$value) {
		$value = str_replace(",", ".", $value);
		$value = number_format((float)$value, 2, ',', '');
		return $value;
	}
	function sp_utils_delete_between($beginning, $end, $string) {
		while(true) {
			$beginningPos = strpos($string, $beginning);
			$endPos = strpos($string, $end);
			if (!$beginningPos || !$endPos) {
				return $string;
			}
			$textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);
			$string = str_replace($textToDelete, '', $string);
		}
		return $string;
	}
	if(!function_exists("sp_utils_generate_random"))
	{
		function sp_utils_generate_random ($length)
		{
			$result = "";

			// define possible characters - any character in this string can be
			// picked for use in the password, so if you want to put vowels back in
			// or add special characters such as exclamation marks, this is where
			// you should do it
			$possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";

			// we refer to the length of $possible a few times, so let's grab it now
			$maxlength = strlen($possible);

			// check for length overflow and truncate if necessary
			if ($length > $maxlength) {
				$length = $maxlength;
			}

			// set up a counter for how many characters are in the password so far
			$i = 0;

			// add random characters to $password until $length is reached
			while ($i < $length) {

				// pick a random character from the possible ones
				$char = substr($possible, mt_rand(0, $maxlength-1), 1);

				// have we already used this character in $password?
				if (!strstr($result, $char)) {
					// no, so it's OK to add it onto the end of whatever we've already got...
					$result .= $char;
					// ... and increase the counter by one
					$i++;
				}

			}

			// done!
			return $result;
		}
	}
?>