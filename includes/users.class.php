<?php
if(!class_exists("users")){
	class users
	{	
		var $prefix = '$2a$';
		var $custo = 10;
		var $salt = 'h1s2a3h4s5i6d7e8m9raps';
	    public function __construct() {
			
	    }
	    
	    public function process() {
			global $API;
        	if ((int)method_exists($this, $API->method) > 0) {
        		return $this->{$API->method}($API->args);
        		if($retval!=false)
					return $API->_response($retval);
        		else
        			return $API->_response("Internal error: $API->method", 500);
        	}
        	return $API->_response("No method: $API->method", 405);
	    }
	    
		protected function put() {
			global $API;
			
			$id = "";
			if(isset($API->put_contents["id"]))
				$id = $API->put_contents["id"];
			
			if(isset($API->args[0])) {
				if($API->args[0]=="clients") {
					$clients = array();
					if(isset($API->put_contents["clients"]))
						$clients = $API->put_contents["clients"];
					
					try{
						$stmt = $API->dbh->prepare("DELETE FROM users_fav_clients WHERE user_id=?");
						$stmt->bindParam(1, $id);
						$stmt->execute();
					}
					catch(PDOException $e) {return $API->_response("Erro da base de dados ao atualizar clientes favoritos", 500);}
					
					if(count($clients)>0)
					{
						$API->dbh->beginTransaction();
						foreach($clients as $client) {
							if(!is_numeric($client)) continue;
							try{
								$stmt = $API->dbh->prepare("INSERT INTO users_fav_clients (user_id,client_id) VALUES (?,?)");
								$stmt->bindParam(1, $id);
								$stmt->bindParam(2, $client);
								$stmt->execute();
							}
							catch(PDOException $e) {
								return $API->_response("Erro ao inserir valores na base de dados", 500);
							}
						}
						$API->dbh->commit();
					}
					
					return $API->_response("Lista de clientes atualizada");
				}
				
				if($API->args[0]=="products") {
					$products = array();
					if(isset($API->put_contents["products"]))
						$products = $API->put_contents["products"];
					
					try{
						$stmt = $API->dbh->prepare("DELETE FROM users_fav_products WHERE user_id=?");
						$stmt->bindParam(1, $id);
						$stmt->execute();
					}
					catch(PDOException $e) {return $API->_response("Erro da base de dados ao atualizar produtos favoritos", 500);}
					
					if(count($products)>0)
					{
						$API->dbh->beginTransaction();
						foreach($products as $product) {
							try{
								$stmt = $API->dbh->exec("INSERT INTO users_fav_products (user_id,product_id) VALUES ('$id','$product')");
							}
							catch(PDOException $e) {
								return $API->_response("Erro ao inserir valores na base de dados", 500);
							}
						}
						$API->dbh->commit();
					}
					
					return $API->_response("Lista de produtos atualizada");
				}
				
				if($API->args[0]=="configs") {
					$configs = array();
					if(isset($API->put_contents["configs"]))
						$configs = $API->put_contents["configs"];

					foreach ($configs as $config) {
						try{
							$stmt = $API->dbh->exec("DELETE FROM configurations WHERE name='".$config["name"]."' AND user_id='".$id."'");
						}
						catch(PDOException $e) {return $API->_response("Error on delete, updating configuration database", 500);}

						try{
							$stmt = $API->dbh->prepare("INSERT INTO configurations (name,user_id,value) VALUES (?,?,?)");

							$stmt->bindParam(1, $config["name"]);
							$stmt->bindParam(2, $id);
							$stmt->bindParam(3, $config["value"]);
							$stmt->execute();
						}
						catch(PDOException $e) {
							return $API->_response("Error updating configuration database", 500);
						}
					}

					$configs_to_read = array("email_notification","automatic_sync");
					$configs = array();
					foreach ($configs_to_read as $config) {
						$result = $API->dbh->query("select * from configurations where name='".$config."_default'");
						if ( $result )	{
							$result = $result->fetch(PDO::FETCH_ASSOC);
							if ( $result )
							{
								$configs[$config] = $result;
							}
						}
						$result = $API->dbh->query("select * from configurations where user_id='".$API->user["id"]."' AND name='".$config."'");
						if ( $result )	{
							$result = $result->fetch(PDO::FETCH_ASSOC);
							if ( $result )
							{
								$configs[$config] = $result;
							}
						}
					}

					return $API->_response($configs);
				}
				
				if($API->args[0]=="profile") {
					$configs = array();
					if(isset($API->put_contents["configs"]))
						$configs = $API->put_contents["configs"];

					foreach ($configs as $config) {
						if($config["name"]=="nome"||$config["name"]=="email"||$config["name"]=="password") {
							try{
								$stmt = $API->dbh->exec("UPDATE users SET ".$config["name"]."='".$config["value"]."' WHERE id='".$id."';");
							}
							catch(PDOException $e) {
								return $API->_response("Erro ao atualizar perfil na base de dados", 500);
							}
						}
					}

					return $API->_response($configs);
				}
			}
			
			$nome = "";
			if(isset($API->put_contents["nome"]))
				$nome = $API->put_contents["nome"];

			$email = "";
			if(isset($API->put_contents["email"]))
				$email = $API->put_contents["email"];
			
			$tipo = "2";
			if(isset($API->put_contents["tipo"]))
				$tipo = $API->put_contents["tipo"];
			
			$notifications = "true";
			if(isset($API->put_contents["notifications"]))
				$notifications = $API->put_contents["notifications"];

			$grupo = "";
			if(isset($API->put_contents["grupo"]))
				$grupo = $API->put_contents["grupo"];

			$client_id = "";
			if(isset($API->put_contents["client_id"]))
				$client_id = $API->put_contents["client_id"];
			
			if ( $nome == "" || $email == "" || $tipo == "" )
				return $API->_response("Existem campos obrigatorios por preencher", 400);
			
			// TODO: validar strings
			$result = $API->dbh->query("select id,nome from users where nome='".$nome."'");
			if ( $result )	{
				$result = $result->fetch(PDO::FETCH_ASSOC);
				if ( $result )
				{
					if($id == "")
						return $API->_response("Utilizador com nome $nome ja existe", 409);
					elseif ($result["id"]!=$id)
						return $API->_response("Utilizador com nome $nome ja existe", 409);
				}
			}
			
			$result = $API->dbh->query("select id,email from users where email='".$email."'");
			if ( $result )	{
				$result = $result->fetch(PDO::FETCH_ASSOC);
				if ( $result )
				{
					if($id == "")
						return $API->_response("Utilizador com email $email ja existe", 409);
					elseif ($result["id"]!=$id)
						return $API->_response("Utilizador com email $email ja existe", 409);
				}
			}

			if($client_id!="") {
				$result = $API->dbh->query("select id,email,client_id from users where client_id='".$client_id."'");
				if ( $result )	{
					$result = $result->fetch(PDO::FETCH_ASSOC);
					if ( $result )
					{
					if($id == "")
						return $API->_response("Ja existe um utilizador com este cliente associado", 409);
					elseif ($result["id"]!=$id)
						return $API->_response("Ja existe um utilizador com este cliente associado", 409);
					}
				}
			}
			
			try{
				if($id != "")
					$stmt = $API->dbh->prepare("UPDATE users SET nome=?,email=?,tipo=?,grupo=?,client_id=? WHERE id='$id'");
				else {
					$new_password = sp_utils_generate_random(8);
					$time = time();
					$hash = crypt($new_password, $this->prefix . 10 . '$' . $this->salt . '$');
					$stmt = $API->dbh->prepare("INSERT INTO users (nome,email,tipo,grupo,client_id,password) VALUES (?,?,?,?,?,'".$hash."')");
				}
				$stmt->bindParam(1, $nome);
				$stmt->bindParam(2, $email);
				$stmt->bindParam(3, $tipo);
				$stmt->bindParam(4, $grupo);
				$stmt->bindParam(5, $client_id);
				$stmt->execute();
				if($id == "")
					$inserted_user_id = $API->dbh->lastInsertId();
			}
			catch(PDOException $e) {
				if($id != "")
					return $API->_response("Database error in edit", 500);
				else
					return $API->_response("Database error in insert", 500);
			}

			try{
				$stmt = $API->dbh->exec("DELETE FROM configurations WHERE name='email_notification' AND user_id='".$id."'");
			}
			catch(PDOException $e) {return $API->_response("Error on delete, updating configuration database", 500);}

			try{
				$stmt = $API->dbh->prepare("INSERT INTO configurations (name,user_id,value) VALUES ('email_notification',?,?)");
				if($id != "") 
					$stmt->bindParam(1, $id);
				else
					$stmt->bindParam(1, $inserted_user_id);
				$stmt->bindParam(2, $notifications);
				$stmt->execute();
			}
			catch(PDOException $e) {
				return $API->_response("Error updating configuration database", 500);
			}

			if($id != "")
				return $API->_response("Utilizador editado com sucesso");
			else {

				$email_content = file_get_contents("includes/utils/email-templates/new_user.html");
				$email_content = mb_convert_encoding($email_content, 'HTML-ENTITIES', "UTF-8");
				$replace = array();
				$replace["@SP_VALOR_USER_NOME@"] = $nome;
				$replace["@SP_VALOR_USER_EMAIL@"] = $email;
				$replace["@SP_VALOR_USER_PASSWORD@"] = $new_password;
				$replace["@SP_VALOR_PLATFORM_URL_PROFILE@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/index.html##modal#profile#open";
				$replace["@SP_VALOR_PLATFORM_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/index.html";

				$email_content = str_replace(array_keys($replace), array_values($replace), $email_content);

				$images = array();
				$images[] = array("path"=>realpath(dirname($_SERVER['SCRIPT_FILENAME']))."/../includes/utils/email-templates/logo.png","name"=>'logo.png',"cid"=>'logo.png');

				if(sp_send_email($email,"web@sparmedix.pt","Web Sparmedix", "Dados de utilizador", $email_content, array(), $images)) {
					return $API->_response("OK");
				} else {
					return $API->_response("Error sending e-mail", 500);
				}

				return $API->_response("Utilizador inserido com sucesso");
			}
		}
		
		protected function delete() {
			global $API;
			
			$id = "";
			if(isset($API->args[0]))
				$id = $API->args[0];
			
			if ( !is_numeric ( $id ) ) return $API->_response("Bad user value ".$id, 400);
			
			try{
				$stmt = $API->dbh->prepare("DELETE FROM users WHERE id=?");
				$stmt->bindParam(1, $id);
				$stmt->execute();
			}
			catch(PDOException $e) {return $API->_response("Database error in delete", 500);}
			
			return $API->_response("Utilizador removido com sucesso");
		}
		
		protected function get() {
			global $API;
			/*
			$this->dbh->exec("INSERT INTO groups (id, nome, descricao) VALUES (0,'Interno','Descricao interno');");
			$this->dbh->exec("INSERT INTO groups (id, nome, descricao) VALUES (1,'Externo Empresa 1','Descricao Externo Empresa 1');");
			$this->dbh->exec("INSERT INTO groups (id, nome, descricao) VALUES (2,'Cliente Farmacia 2','Descricao Cliente Farmacia 2');");
			*/
			$configs_to_read = array("email_notification","automatic_sync");
				
			if ( count ( $API->args ) > 0 ) {
				if($API->user["tipo"]!="0") $id = $API->user["id"];
				else $id = $API->args[0];
				if ( !is_numeric ( $id ) ) return $API->_response("Bad user value ".$id, 400);
				$result = $API->dbh->query("select id,nome,email,tipo,grupo from users where id='".$id."'");
				if ( $result )	{
					$user = $result->fetch(PDO::FETCH_ASSOC);
					if ( $user )
					{
						$user["configs"] = array();
						foreach ($configs_to_read as $config) {
							$result = $API->dbh->query("select * from configurations where name='".$config."_default'");
							if ( $result )	{
								$result = $result->fetch(PDO::FETCH_ASSOC);
								if ( $result )
								{
									$user["configs"][$config] = $result;
								}
							}

							$result = $API->dbh->query("select * from configurations where user_id='".$id."' AND name='".$config."'");
							if ( $result )	{
								$result = $result->fetch(PDO::FETCH_ASSOC);
								if ( $result )
								{
									$user["configs"][$config] = $result;
								}
							}
						}

						return $API->_response($user);
					}
					else {
						return $API->_response("User not found: id ".$id, 404);
					}
				}
			}
			else if ( $API->user["tipo"]!="0") {
				$id = $API->user["id"];
				if ( !is_numeric ( $id ) ) return $API->_response("Bad user value ".$id, 400);
				$result = $API->dbh->query("select id,nome,email,tipo,grupo from users where id='".$id."'");
				if ( $result )	{
					$user = $result->fetchAll(PDO::FETCH_ASSOC);
					if ( $user )
					{
						$user["configs"] = array();
						foreach ($configs_to_read as $config) {
							$result = $API->dbh->query("select * from configurations where name='".$config."_default'");
							if ( $result )	{
								$result = $result->fetch(PDO::FETCH_ASSOC);
								if ( $result )
								{
									$user["configs"][$config] = $result;
								}
							}

							$result = $API->dbh->query("select * from configurations where user_id='".$id."' AND name='".$config."'");
							if ( $result )	{
								$result = $result->fetch(PDO::FETCH_ASSOC);
								if ( $result )
								{
									$user["configs"][$config] = $result;
								}
							}
						}
						return $API->_response($user);
					}
					else {
						return $API->_response("User not found: id ".$id, 404);
					}
				}
			} else {
				$result = $API->dbh->query("select id,nome,email,tipo,grupo,client_id from users");
				if ( $result ) {
					$users = $result->fetchAll(PDO::FETCH_ASSOC);
					if( count ( $users ) > 0 ) {
						foreach ($users as &$user) {
							$user["configs"] = array();
							foreach ($configs_to_read as $config) {
								$result = $API->dbh->query("select * from configurations where name='".$config."_default'");
								if ( $result )	{
									$result = $result->fetch(PDO::FETCH_ASSOC);
									if ( $result )
									{
										$user["configs"][$config] = $result;
									}
								}

								$result = $API->dbh->query("select * from configurations where user_id='".$user["id"]."' AND name='".$config."'");
								if ( $result )	{
									$result = $result->fetch(PDO::FETCH_ASSOC);
									if ( $result )
									{
										$user["configs"][$config] = $result;
									}
								}
							}
						}
						return $API->_response($users);
					}
					else {
						return $API->_response(array());
					}
				}
			}
			return $API->_response("Error fetching user information", 500);
		}
	}
}
?>