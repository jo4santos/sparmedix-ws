<?php
if(!class_exists("groups")){
	class groups
	{	
	    public function __construct() {
			
	    }
	    
	    public function process() {
			global $API;
        	if ((int)method_exists($this, $API->method) > 0) {
        		return $this->{$API->method}($API->args);
        		if($retval!=false)
					return $API->_response($retval);
        		else
        			return $API->_response("Internal error: $API->method", 500);
        	}
        	return $API->_response("No method: $API->method", 405);
	    }
	    
		protected function put() {
			global $API;

			$id = "";
			if(isset($API->put_contents["id"]))
				$id = $API->put_contents["id"];
			
			$nome = "";
			if(isset($API->put_contents["nome"]))
				$nome = $API->put_contents["nome"];

            $descricao = "";
            if(isset($API->put_contents["descricao"]))
                $descricao = $API->put_contents["descricao"];

            $lista_distribuicao = "";
            if(isset($API->put_contents["lista_distribuicao"]))
                $lista_distribuicao = $API->put_contents["lista_distribuicao"];

			$tipo = "2";
			if(isset($API->put_contents["tipo"]))
				$tipo = $API->put_contents["tipo"];

			$produtos = array();
			if(isset($API->put_contents["produtos"]))
				$produtos = $API->put_contents["produtos"];
			if(!is_array($produtos))
				$produtos = explode(",",$produtos);

			$clientes = array();
			if(isset($API->put_contents["clientes"]))
				$clientes = $API->put_contents["clientes"];
			if(!is_array($clientes))
				$clientes = explode(",",$clientes);
			
			if ( $nome == "" || $descricao == "" )
				return $API->_response("Existem campos obrigatorios por preencher", 400);
			
			$result = $API->dbh->query("select id,nome from groups where nome='".$nome."'");
			if ( $result )	{
				$result = $result->fetch(PDO::FETCH_ASSOC);
				if ( $result )
				{
					if($id == "")
						return $API->_response("Grupo com nome $nome ja existe", 409);
					elseif ($result["id"]!=$id)
						return $API->_response("Grupo com nome $nome ja existe", 409);
				}
			}
			
			try{
				if($id != "")
					$stmt = $API->dbh->prepare("UPDATE groups SET nome=?,descricao=?,lista_distribuicao=?,tipo=? WHERE id='$id'");
				else
					$stmt = $API->dbh->prepare("INSERT INTO groups (nome,descricao,lista_distribuicao,tipo) VALUES (?,?,?,?)");
				$stmt->bindParam(1, $nome);
                $stmt->bindParam(2, $descricao);
                $stmt->bindParam(3, $lista_distribuicao);
				$stmt->bindParam(4, $tipo);
				$stmt->execute();
			}
			catch(PDOException $e) {
				if($id != "")
					return $API->_response("Database error in edit", 500);
				else
					return $API->_response("Database error in insert", 500);
			}
			
			if($id == "") {
				$inserted_group_id = $API->dbh->lastInsertId();
				if(count($clientes)>0)
				{
					foreach($clientes as $cliente) {
						if(!is_numeric($cliente)) continue;
						try{
							$stmt = $API->dbh->prepare("INSERT INTO groups_clients (group_id,client_id) VALUES (?,?)");
							$stmt->bindParam(1, $inserted_group_id);
							$stmt->bindParam(2, $cliente);
							$stmt->execute();
						}
						catch(PDOException $e) {
							return $API->_response("Database error in insert", 500);
						}
					}
				}
				if(count($produtos)>0)
				{
					foreach($produtos as $produto) {
						if(!is_numeric($produto)) continue;
						try{
							$stmt = $API->dbh->prepare("INSERT INTO groups_products (group_id,product_id) VALUES (?,?)");
							$stmt->bindParam(1, $inserted_group_id);
							$stmt->bindParam(2, $produto);
							$stmt->execute();
						}
						catch(PDOException $e) {
							return $API->_response("Database error in insert", 500);
						}
					}
				}
			}
			else
			{
				try{
					$stmt = $API->dbh->prepare("DELETE FROM groups_clients WHERE group_id=?");
					$stmt->bindParam(1, $id);
					$stmt->execute();
				}
				catch(PDOException $e) {return $API->_response("Database error in delete", 500);}
					
				try{
					$stmt = $API->dbh->prepare("DELETE FROM groups_products WHERE group_id=?");
					$stmt->bindParam(1, $id);
					$stmt->execute();
				}
				catch(PDOException $e) {return $API->_response("Database error in delete", 500);}
				
				if(count($clientes)>0)
				{
					foreach($clientes as $cliente) {
						if(!is_numeric($cliente)) continue;
						try{
							$stmt = $API->dbh->prepare("INSERT INTO groups_clients (group_id,client_id) VALUES (?,?)");
							$stmt->bindParam(1, $id);
							$stmt->bindParam(2, $cliente);
							$stmt->execute();
						}
						catch(PDOException $e) {
							return $API->_response("Database error in insert", 500);
						}
					}
				}
				if(count($produtos)>0)
				{
					foreach($produtos as $produto) {
						if(!is_numeric($produto)) continue;
						try{
							$stmt = $API->dbh->prepare("INSERT INTO groups_products (group_id,product_id) VALUES (?,?)");
							$stmt->bindParam(1, $id);
							$stmt->bindParam(2, $produto);
							$stmt->execute();
						}
						catch(PDOException $e) {
							return $API->_response("Database error in insert", 500);
						}
					}
				}
				try{
					$stmt = $API->dbh->prepare("UPDATE users SET grupo=null WHERE grupo='$id' AND tipo<>'$tipo'");
					$stmt->execute();
				}
				catch(PDOException $e) {
					return $API->_response("Database error in edit", 500);
				}
			}
			
			if($id != "")
				return $API->_response("Grupo editado com sucesso");
			else
				return $API->_response("Grupo inserido com sucesso");	
			
			return $API->_response(print_r($API->put_contents,true));
		}
		
		protected function get() {
			global $API;

            /*try{
                $stmt = $API->dbh->exec("ALTER TABLE groups ADD COLUMN lista_distribuicao VARCHAR(512) DEFAULT '';");
                return $API->_response("OK");
            }
            catch(PDOException $e) {
                return $API->_response("NOK", 500);
            }
            return $API->_response("Adicionada coluna", 500);*/
			
			/*
			$this->dbh->exec("INSERT INTO groups (id, nome, descricao) VALUES (0,'Interno','Descricao interno');");
			$this->dbh->exec("INSERT INTO groups (id, nome, descricao) VALUES (1,'Externo Empresa 1','Descricao Externo Empresa 1');");
			$this->dbh->exec("INSERT INTO groups (id, nome, descricao) VALUES (2,'Cliente Farmacia 2','Descricao Cliente Farmacia 2');");
			*/
			
			if ( count ( $API->args ) > 0 ) {
				if ( !is_numeric ( $API->args[0] ) ) return $API->_response("Bad group value ".$API->args[0], 400);
				$result = $API->dbh->query("select * from groups where id='".$API->args[0]."'");
				if ( $result )	{
					$result = $result->fetch(PDO::FETCH_ASSOC);
					if ( $result )
					{
						$result["clientes"] = $this->get_group_clients($API->args[0]);
						$result["produtos"] = $this->get_group_products($API->args[0]);
						return $API->_response($result);
					}
					else {
						return $API->_response("Group not found: id ".$API->args[0], 404);
					}
				}
			}
			else {
				$result = $API->dbh->query("select * from groups");
				if ( $result ) {
					$groups = $result->fetchAll(PDO::FETCH_ASSOC);
					if( count ( $groups ) > 0 ) {
						foreach ( $groups as &$group )
						{
							$group["clientes"] = $this->get_group_clients($group["id"]);
							$group["produtos"] = $this->get_group_products($group["id"]);
						}
						return $API->_response($groups);
					}
					else {
						return $API->_response(array());
					}
				}
			}
			return $API->_response("Error fetching group information", 500);
		}
		protected function get_group_clients ($id) {
			global $API;		
			$result = $API->dbh->query("select * from groups_clients WHERE group_id='".$id."'");
			if ( $result ) {
				$clients = $result->fetchAll(PDO::FETCH_ASSOC);
				if( count ( $clients ) > 0 ) {
					$client_ids = array();
					foreach ( $clients as $client ) {
						$client_ids[] = $client["client_id"];
					}
					return $client_ids;
				}
				else {
					return array();
				}
			}			
			return array();
		}
		protected function get_group_products ($id) {
			global $API;		
			$result = $API->dbh->query("select * from groups_products WHERE group_id='".$id."'");
			if ( $result ) {
				$products = $result->fetchAll(PDO::FETCH_ASSOC);
				if( count ( $products ) > 0 ) {
					$product_ids = array();
					foreach ( $products as $product ) {
						$product_ids[] = $product["product_id"];
					}
					return $product_ids;
				}
				else {
					return array();
				}
			}			
			return array();
		}
		
		protected function delete() {
			global $API;

			$id = "";
			if(isset($API->args[0]))
				$id = $API->args[0];
			
			if ( !is_numeric ( $id ) ) return $API->_response("Bad group value ".$id, 400);
			
			try{
				$stmt = $API->dbh->prepare("DELETE FROM groups WHERE id=?");
				$stmt->bindParam(1, $id);
				$stmt->execute();
			}
			catch(PDOException $e) {return $API->_response("Database error in delete", 500);}
			
			try{
				$stmt = $API->dbh->prepare("DELETE FROM groups_clients WHERE group_id=?");
				$stmt->bindParam(1, $id);
				$stmt->execute();
			}
			catch(PDOException $e) {return $API->_response("Database error in delete", 500);}
			
			try{
				$stmt = $API->dbh->prepare("DELETE FROM groups_products WHERE group_id=?");
				$stmt->bindParam(1, $id);
				$stmt->execute();
			}
			catch(PDOException $e) {return $API->_response("Database error in delete", 500);}

			try{
				$stmt = $API->dbh->prepare("UPDATE users SET grupo=null WHERE grupo='$id'");
				$stmt->execute();
			}
			catch(PDOException $e) {
				return $API->_response("Database error in edit", 500);
			}
			
			return $API->_response("Grupo removido com sucesso");
		}
	}
}
?>