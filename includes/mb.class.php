<?php
if(!class_exists("mb")){
	class mb
	{
	    public function __construct() {
			
	    }
	    
	    public function process() {
			global $API;
        	if ((int)method_exists($this, $API->method) > 0) {
        		return $this->{$API->method}($API->args);
        		if($retval!=false)
					return $API->_response($retval);
        		else
        			return $API->_response("Internal error: $API->method", 500);
        	}
        	return $API->_response("No method: $API->method", 405);
	    }
	    
		protected function get() {
			global $API;
			
// try{
//   				$stmt = $API->dbh->exec("ALTER TABLE notes ADD COLUMN referencia_payment varchar(256);");
//  			}
//  			catch(PDOException $e) {
//  				return $API->_response("Erro ao atualizar produto na base de dados", 500);
//  			}
//  			return $API->_response("Erro ao atualizar produto na base de dados", 500);

			if(isset($API->args[0])) {
				if($API->args[0] == "debug") {
					$query = "select * from notes where estado='1';";
					$result = $API->dbh->query($query);
					if ( $result )	{
						$notes = $result->fetchAll(PDO::FETCH_ASSOC);
						foreach($notes as $note) {
							echo 'http://'.$_SERVER["HTTP_HOST"].'/ws/mb/?chave=Z.ZG9NgXK5T4CvnXsS0QNJJSX42ka/G&entidade=11202&referencia='.$note["referencia"].'&valor='.$note["preco"].'&datahorapag='.date("Y-m-d H:i:s",time()).'&terminal=Android'."\n\n";
						}
					}
					exit();
				}
				if($API->args[0] == "mail") {
					$this->send_payment_email($API->args[1]);
					exit();
				}
			}
			$chave = $entidade = $referencia = $valor = $datahorapag = $terminal = "";
			
			$chave 			= ( isset ( $API->request["chave"] ) ? $API->request["chave"] : "" );
			$entidade 		= ( isset ( $API->request["entidade"] ) ? $API->request["entidade"] : "" );
			$referencia 	= ( isset ( $API->request["referencia"] ) ? $API->request["referencia"] : "" );
			$valor 			= ( isset ( $API->request["valor"] ) ? sp_utils_format_price($API->request["valor"]) : "" );
			$datahorapag 	= ( isset ( $API->request["datahorapag"] ) ? $API->request["datahorapag"] : "" );
			$terminal 		= ( isset ( $API->request["terminal"] ) ? $API->request["terminal"] : "" );
			
			echo "chave:".$chave."\n";
			echo "entidade:".$entidade."\n";
			echo "referencia:".$referencia."\n";
			echo "valor:".$valor."\n";
			echo "datahorapag:".$datahorapag."\n";
			echo "terminal:".$terminal."\n";
			
			if($chave.$entidade.$referencia.$valor.$datahorapag == "") {
				return $API->_response("Erro, parametros obrigatorios em falta", 400);
			}
			if($entidade!="11202") {
				return $API->_response("Erro, entidade invalida", 400);
			}
			$query = "select * from notes where referencia='".$referencia."';";				
			$result = $API->dbh->query($query);
			if ( $result )	{
				$result = $result->fetch(PDO::FETCH_ASSOC);
				if ( $result )
				{
					$note = $result;
					if(sp_utils_format_price($valor) != sp_utils_format_price($note["preco"])) {
						$API->log_action(array(
							"type" => "mb_payment",
							"status" => 0,
							"note_id" => $note["id"],
							"user_id" => $note["user_id"],
							"client_id" => $note["client_id"],
							"comment" => "Valor errado: ".$note["codigo"]."|".$entidade."|".$referencia."|".$valor."|".$datahorapag
						));
						return $API->_response("Erro, valor errado", 400);
					}
					if($note["estado"]!=1) {
						$API->log_action(array(
							"type" => "mb_payment",
							"status" => 0,
							"note_id" => $note["id"],
							"user_id" => $note["user_id"],
							"client_id" => $note["client_id"],
							"comment" => "Referencia nao esta em pagamento: ".$note["codigo"]."|".$entidade."|".$referencia."|".$valor."|".$datahorapag
						));
						return $API->_response("Erro, referencia nao esta em pagamento", 400);
					}
					
					try{
						$stmt = $API->dbh->exec("UPDATE notes SET estado='4',data_payment='$datahorapag',referencia_payment='$referencia' WHERE id='".$note["id"]."'");
					}
					catch(PDOException $e) {
						return $API->_response("Erro, base de dados ao editar", 500);
					}
					
					try{
						$stmt = $API->dbh->exec("UPDATE notes SET referencia='' WHERE id='".$note["id"]."'");
					}
					catch(PDOException $e) {
						return $API->_response("Erro, base de dados ao editar", 500);
					}
					
					$API->log_action(array(
						"type" => "mb_payment",
						"status" => 1,
						"note_id" => $note["id"],
						"user_id" => $note["user_id"],
						"client_id" => $note["client_id"],
						"comment" => $note["codigo"]."|".$entidade."|".$referencia."|".$valor."|".$datahorapag
					));

					$this->send_order_email($note["id"],true,4,$referencia);

					return $API->_response($note["client_id"]);
				}
				else {
					$API->log_action(array(
						"type" => "mb_payment",
						"status" => 0,
						"comment" => "Referencia nao encontrada: ".$entidade."|".$referencia."|".$valor."|".$datahorapag
					));
					return $API->_response("Erro, referencia nao encontrada", 404);
				}
			} else {
				$API->log_action(array(
					"type" => "mb_payment",
					"status" => 0,
					"comment" => "Referencia nao encontrada: ".$entidade."|".$referencia."|".$valor."|".$datahorapag
				));
				return $API->_response("Erro, referencia nao encontrada", 404);
			}
			
			
//			/ws/mb/?chave=Z.ZG9NgXK5T4CvnXsS0QNJJSX42ka/G&entidade=11202&referencia=100100100&valor=100&datahorapag=04-02-2014 00:38:40&terminal=Android
			
			return $API->_response("Atualizado");
			return $API->_response("Erro ao obter informacoes de clientes", 500);
		}
		protected function send_order_email($note_id,$update = false,$next=false,$referencia) {
			global $API;

			$query = "select * from notes where id='".$note_id."'";
			$result = $API->dbh->query($query);
			if ( $result )	{
				$result = $result->fetch(PDO::FETCH_ASSOC);
				if ( $result )
				{
					foreach ($result as &$val) $val = urldecode($val);
					$result["products"] = $API->get_note_products($note_id);
					$note = $result;
				}
				else {
					return false;
				}
			}

			$client = array();
            $result = $API->dbh->query("select * from clients WHERE id='".$note["client_id"]."'");
            if ( $result ) {
                $client = $result->fetch(PDO::FETCH_ASSOC);
                foreach ($client as &$val) $val = urldecode($val);
            }

            $payee_client = array();
            $result = $API->dbh->query("select * from clients WHERE id='".$note["payee_id"]."'");
            if ( $result ) {
                $payee_client = $result->fetch(PDO::FETCH_ASSOC);
               foreach ($payee_client as &$val) $val = urldecode($val);
            }
			
			$user = array();
			$result = $API->dbh->query("select * from users WHERE id='".$note["user_id"]."'");
			if ( $result ) $user = $result->fetch(PDO::FETCH_ASSOC);
			$users_to_notify = array();
		
			$result = $API->dbh->query("select * from configurations where user_id='".$user["id"]."' AND name='email_notification'");
			if ( $result )	{
				$result = $result->fetch(PDO::FETCH_ASSOC);
				if ( $result )
					if($result["value"]=="true")
						$users_to_notify[] = array("type"=>"seller","email"=>$user["email"],"nome"=>$user["nome"],"id"=>$user["id"]);
			}
			
			$send = true;
			$result = $API->dbh->query("select * from users where client_id='".$client["id"]."'");
			if ( $result )	{
				$result = $result->fetch(PDO::FETCH_ASSOC);
				if ( $result ) {
					$result = $API->dbh->query("select * from configurations where user_id='".$result["id"]."' AND name='email_notification'");
					if ( $result )	{
						$result = $result->fetch(PDO::FETCH_ASSOC);
						if ( $result )
							if($result["value"]=="false") {
								$send = false;
							}
					}
				}
			}
			if($send) {
				$users_to_notify[] = array("type"=>"client","email"=>$client["email"],"nome"=>$client["nome"],"id"=>$client["id"]);
			}
			$result = $API->dbh->query("select * from users where tipo='0'");
			if ( $result )	{
				$users = $result->fetchAll(PDO::FETCH_ASSOC);
				if ( $users ) {
					foreach ($users as $user_mail) {
						$send = true;
						$result = $API->dbh->query("select * from configurations where user_id='".$user_mail["id"]."' AND name='email_notification'");
						if ( $result )	{
							$result = $result->fetch(PDO::FETCH_ASSOC);
							if ( $result )
								if($result["value"]=="false") {
									$send = false;
								}
						}
						if($send) {
							$users_to_notify[] = array("type"=>"admin","email"=>$user_mail["email"],"nome"=>$user_mail["nome"],"id"=>$user_mail["id"]);
						}
					}
				}
			}

            $lista_distribuicao = "";
            $result = $API->dbh->query("select nome,lista_distribuicao from groups WHERE id='".$user["grupo"]."'");
            if ( $result ) {
                $result = $result->fetchAll(PDO::FETCH_ASSOC);
                if(is_array($result)) $result = $result[0];
                $user["grupo_nome"] = $result["nome"];
                $lista_distribuicao = explode(";",$result["lista_distribuicao"]);
            }

			$log_options = array();
			$log_options["type"] = "note_new";
			$log_options["note_id"] = $note_id;
			$log_options["comment"] = $note["codigo"]."|Encomenda";
			$log_options["client_id"] = $note["client_id"];
			$log_options["user_id"] = $note["user_id"];
			$log_options["status"] = 1;
			$API->log_action($log_options);
            if ($note["client_id"] != $note["payee_id"]) {
                $email_content = file_get_contents("includes/utils/email-templates/encomenda_email_dif.html");
            }
            else {
                $email_content = file_get_contents("includes/utils/email-templates/encomenda_email.html");
            }
			$email_content = mb_convert_encoding($email_content, 'HTML-ENTITIES', "UTF-8");
			$replace = array();
			$replace["@VALOR_NOTA_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/#dashboard#modal#note#".$note_id;
			$replace["@VALOR_NOTA_CODIGO@"] = $note["codigo"];
			$replace["@VALOR_NOTA_DATA_CRIACAO@"] = $note["data"];
			$replace["@VALOR_NOTA_PRECO@"] = sp_utils_format_price($note["preco"]);
			$replace["@VALOR_VENDEDOR_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/#dashboard#modal#user#".$note["user_id"];
			$replace["@VALOR_VENDEDOR_NOME@"] = $note["user_nome"];
			$replace["@VALOR_VENDEDOR_GRUPO@"] = $user["grupo_nome"];
			$replace["@VALOR_VENDEDOR_EMAIL@"] = $user["email"];
			
			$replace["@VALOR_CLIENTE_FINAL_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/#dashboard#modal#client#".$note["client_id"];
			$replace["@VALOR_CLIENTE_FINAL_NOME@"] = $client["nome"];
			$replace["@VALOR_CLIENTE_FINAL_CODIGO@"] = $client["codigo"];
			$replace["@VALOR_CLIENTE_FINAL_EMAIL@"] = $client["email"];
			$replace["@VALOR_CLIENTE_FINAL_NIF@"] = $client["nif"];
			$replace["@VALOR_CLIENTE_FINAL_RESPONSAVEL@"] = $client["responsavel"];
            $replace["@VALOR_CLIENTE_FINAL_MORADA@"] = $client["morada"]."<br>".$client["cdpostal"]." ".$client["local"];
			
			$replace["@VALOR_CLIENTE_FATURADO_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/#dashboard#modal#client#".$note["payee_id"];
			$replace["@VALOR_CLIENTE_FATURADO_NOME@"] = $payee_client["nome"];
			$replace["@VALOR_CLIENTE_FATURADO_CODIGO@"] = $payee_client["codigo"];
			$replace["@VALOR_CLIENTE_FATURADO_EMAIL@"] = $payee_client["email"];
			$replace["@VALOR_CLIENTE_FATURADO_NIF@"] = $payee_client["nif"];
			$replace["@VALOR_CLIENTE_FATURADO_RESPONSAVEL@"] = $payee_client["responsavel"];
            $replace["@VALOR_CLIENTE_FATURADO_MORADA@"] = $payee_client["morada"]."<br>".$payee_client["cdpostal"]." ".$payee_client["local"];
			
			$replace["@VALOR_PRODUTOS@"] = "";
			$replace["@VALOR_NOTA_PRECO_SEM_IVA@"] = 0;
			$replace["@VALOR_NOTA_TOTAL_IVA@"] = 0;
			foreach($note["products"] as $produto) {
				$replace["@VALOR_PRODUTOS@"] .= '		<tr>';
				$replace["@VALOR_PRODUTOS@"] .= '		<td align="left" style="border:solid 1px #ccc;line-height:14px;"><span style="color:#777;font-size:10px;">'.(strlen($produto["codigo"])>7?"EAN":"CNP").':</span>&nbsp;&nbsp; <a href="'."http://".$_SERVER["HTTP_HOST"]."/areacliente/##modal#product#".$produto["id"].'#open" style="color:#008dd2;text-decoration:underline;">'.$produto["codigo"].'</a><br /><span style="color:#777;font-size:10px;">Nome:</span> '.$produto["product_nome"].'</td>';
				$replace["@VALOR_PRODUTOS@"] .= '		<td align="right" style="border:solid 1px #ccc;line-height:28px;" nowrap>'.$produto["quantidade"].($produto["bonus"]>0?"+".$produto["bonus"]:"").'</td>';
				if(!($note["client_id"] != $note["payee_id"])) {
                    $replace["@VALOR_PRODUTOS@"] .= '		<td align="right" style="border:solid 1px #ccc;line-height:28px;" nowrap>' . $produto["preco"] . '&euro;</td>';
                    $replace["@VALOR_PRODUTOS@"] .= '		<td align="right" style="border:solid 1px #ccc;line-height:28px;" nowrap>' . $produto["iva"] . '%</td>';
                    $preco = $produto["preco"] * $produto["quantidade"];
                    sp_utils_format_price($preco);
                    $replace["@VALOR_PRODUTOS@"] .= '		<td align="right" style="border:solid 1px #ccc;width:90px;line-height:28px;" nowrap>' . $preco . '&euro;</td>';
                }
				$replace["@VALOR_PRODUTOS@"] .= '		</tr>';

				$replace["@VALOR_NOTA_PRECO_SEM_IVA@"] += $produto["preco"]*$produto["quantidade"];
				$replace["@VALOR_NOTA_TOTAL_IVA@"] += $produto["preco"]*$produto["quantidade"]*($produto["iva"]/100);
			}
			sp_utils_format_price($replace["@VALOR_NOTA_PRECO_SEM_IVA@"]);
			sp_utils_format_price($replace["@VALOR_NOTA_TOTAL_IVA@"]);
			$replace["@VALOR_ADMIN_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/#dashboard#modal#configuration#open";
			$replace["@VALOR_PLATFORM_URL@"] = "http://".$_SERVER["HTTP_HOST"]."/areacliente/";

			$type = pathinfo("includes/utils/email-templates/logo.png", PATHINFO_EXTENSION);
			$data = file_get_contents("includes/utils/email-templates/logo.png");
			$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

			$replace["@VALOR_LOGO@"] = $base64;

			if($update) {
				if($next==1) {
					$next = "Em pagamento";
					$next_cor = "#f0ad4e";
				}
				else if($next==2) {
					$next = "Pago, em progresso";
					$next_cor = "#428bca";
					$replace["@VALOR_NOTA_ESTADO_DESCRICAO@"] = "Em ".$note["data_payment"]." por Sparmedix. Pagamento da referência ".$referencia.".";
					$subject = "[ENC][".$note["codigo"]."][PAGO]".$note["client_nome"];
				}
				else if($next==3) {
					$next = "Rejeitada";
					$next_cor = "#d9534f";
				}
				else if($next==4) {
					$next = "Tratada";
					$next_cor = "#5cb85c";
					$subject = "[ENC][".$note["codigo"]."][TRATADA]".$note["client_nome"];
					$replace["@VALOR_NOTA_ESTADO_DESCRICAO@"] = "Em <b>".$note["data_payment"]."</b> por <b>Sparmedix</b>. Pagamento da referência ".$referencia.".";
				}
				$replace["@VALOR_NOTA_ESTADO_PROXIMO@"] = $next;
				$replace["@VALOR_NOTA_ESTADO_PROXIMO_COR@"] = $next_cor;
			}

			$email_content = str_replace(array_keys($replace), array_values($replace), $email_content);

			if($note["payee_id"] == $note["client_id"]) $email_content = sp_utils_delete_between("<!-- TO REMOVE FATURADO START -->","<!-- TO REMOVE FATURADO END -->",$email_content);
			else $email_content = sp_utils_delete_between("<!-- TO REMOVE FINAL START -->","<!-- TO REMOVE FINAL END -->",$email_content);

			if(!$update) {
				$email_content = sp_utils_delete_between("<!-- TO REMOVE ATUALIZACAO START -->","<!-- TO REMOVE ATUALIZACAO END -->",$email_content);
			}

			$API->generate_pdf($note_id);

			$files = array();
			$files[] = array("path"=>realpath(dirname($_SERVER['SCRIPT_FILENAME']))."/../database/orders/".$note["codigo"].'.pdf',"name"=>$note["codigo"].'.pdf');

			$images = array();
			$images[] = array("path"=>realpath(dirname($_SERVER['SCRIPT_FILENAME']))."/../includes/utils/email-templates/logo.png","name"=>'logo.png',"cid"=>'logo.png');

			$email_to_send = array();
			foreach ($users_to_notify as $user) {
				if(!in_array($user["email"], $email_to_send )) {
					$email_to_send[] = $user["email"];
					$log_options = array();
					$log_options["type"] = "note_new_email";
					$log_options["note_id"] = $note_id;
					$log_options["comment"] = $note["codigo"]."|".$user["email"]."|Encomenda";
					if($user["type"]=="client") {
						$log_options["client_id"] = $user["id"];
					} elseif($user["type"]=="seller") {
						$log_options["user_id"] = $user["id"];
					} else {
						$log_options["user_id"] = $user["id"];
					}
					if(sp_send_email($user["email"],"web@sparmedix.pt","Web Sparmedix", $subject, $email_content, $files, $images)) {
						$log_options["status"] = 1;
					} else {
						$log_options["status"] = 0;
					}
					$API->log_action($log_options);
				}
			}
            foreach ($lista_distribuicao as $email) {
                if(!in_array($email, $email_to_send )) {
                    $email_to_send[] = $email;
                    sp_send_email($email,"web@sparmedix.pt","Web Sparmedix", $subject, $email_content, $files, $images);
                }
            }
		}
		protected function get_note_products ($id) {
			global $API;		
			$result = $API->dbh->query("select * from notes_products WHERE note_id='".$id."'");
			if ( $result ) {
				$products = $result->fetchAll(PDO::FETCH_ASSOC);
				if( count ( $products ) > 0 ) {
					foreach ($products as &$product) foreach ($product as &$val) $val = urldecode($val);
					return $products;
				}
				else {
					return array();
				}
			}
			return array();
		}
		protected function get_client_details ($id) {
			global $API;		
			$result = $API->dbh->query("select * from clients WHERE id='".$id."'");
			if ( $result ) {
				$result = $result->fetch(PDO::FETCH_ASSOC);
				foreach ($result as &$val) $val = urldecode($val);
				return $result;
			}
			return false;
		}
	}
}
?>