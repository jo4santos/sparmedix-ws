<?php
require_once '../includes/utils/utils.php';
require_once '../includes/API.class.php';
require_once '../includes/MAIN.class.php';
 
// Requests from the same server don't have a HTTP_ORIGIN header
if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
	$_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
}
try {
	GLOBAL $API;
//error_log("[SP_INDEX_ACCESS] ".$_REQUEST['_url']);
	$API = new MAIN($_REQUEST['_url'], $_SERVER['HTTP_ORIGIN']);
	echo $API->processAPI();
//error_log("[SP_INDEX_RESPONSE] ".$_REQUEST['_url']);
} catch (InvalidArgumentException $e) {
	header("HTTP/1.1 403 Forbidden");
	echo $e->getMessage();
	exit();
} catch (UnexpectedValueException $e) {
	header("HTTP/1.1 401 Unauthorized");
	echo $e->getMessage();
	exit();
} catch (OverflowException $e) {
	header("HTTP/1.1 419 Authentication Timeout");
	echo $e->getMessage();
	exit();
}  catch (LogicException $e) {
	header("HTTP/1.1 200 OK");
	echo $e->getMessage();
	exit();
} catch (Exception $e) {
	header("HTTP/1.1 500 Server Internal Error");
	echo $e->getMessage();
	exit();
}
?>